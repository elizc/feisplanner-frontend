import React, {Component} from "react";
import {connect} from "react-redux";
import PropTypes from "prop-types";
import {Translate} from "react-redux-i18n";
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  FormControl,
  FormHelperText,
  InputLabel,
  MenuItem,
  Select,
  Typography
} from "@material-ui/core";
import ThemeProvider from "@material-ui/styles/ThemeProvider";
import * as jwt from "jsonwebtoken";
import DanceControls from "../../atoms/dance-control/DanceControls";
import {addEntry, editEntry} from "../../organisms/teacher-entries-table/actions";
import {getFeises} from "../../organisms/teacher-feises-table/actions";
import theme from "../../utils/theme";
import {exists, notEmpty} from "../../utils/formCheckers";
import {constants} from "../../utils/constants";
import {isEqual} from "../../utils/comparator";
import {getStudents} from "../../organisms/students-table/actions";

class EntryForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      entryError: false,
      student: {
        value: null,
        errorText: '',
        checkers: [exists]
      },
      studentName: '',
      feis: {
        value: null,
        errorText: '',
        checkers: [exists]
      },
      feisName: '',
      soloDances: {
        value: [],
        errorText: '',
        checkers: [notEmpty]
      }
    };
    this.setFeis = this.setFeis.bind(this);
    this.setStudent = this.setStudent.bind(this);
    this.setDance = this.setDance.bind(this);
    this.addEntry = this.addEntry.bind(this);
    this.editEntry = this.editEntry.bind(this);
    this.close = this.close.bind(this);
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    const entryItemToString = (entryItem) => {
      const {name, level} = entryItem.dance;
      return `${name} ${level}`;
    };
    if (prevProps.entry !== this.props.entry) {
      const {student, feis, soloDances} = prevState;
      const {entry} = this.props;

      this.setState({
        student: {
          ...student,
          value: entry ? entry.student : null
        },
        studentName: entry ? `${entry.student.surname} ${entry.student.firstName}` : '',
        feis: {
          ...feis,
          value: entry ? entry.feis : null
        },
        feisName: entry ? entry.feis.name : '',
        soloDances: {
          ...soloDances,
          value: entry ? entry.entryItems.map(entryItemToString) : []
        },
      })
    }

    if (!isEqual(prevProps, this.props) && this.props.error) {
      this.setState({
        entryError: true
      });
    }
  }

  componentDidMount() {
    const {getStudents, getFeises} = this.props;
    const token = localStorage.getItem('token');
    const parsedToken = jwt.verify(token, constants.JWT_KEY);
    const userId = parsedToken[constants.USER_ID_KEY];

    getStudents(userId);
    getFeises();
  }

  setFeis(event) {
    const {target} = event;
    const feisName = target.value;
    const feis = this.props.feises.find(feis => feis.name === feisName);

    this.setState(prevState => ({
      entryError: false,
      feisName,
      feis: {
        ...prevState.feis,
        value: feis,
      }
    }))
  }

  setStudent(event) {
    const {target} = event;
    const studentName = target.value;
    const [surname, firstName] = studentName.split(' ');
    const student = this.props.students.find(student => student.firstName === firstName
      && student.surname === surname);

    this.setState(prevState => ({
      entryError: false,
      studentName,
      student: {
        ...prevState.student,
        value: student
      }
    }))
  }

  setDance(event) {
    const {target} = event;
    const {name, checked} = target;

    if (checked) {
      this.setState(prevState => ({
        soloDances: {
          ...prevState.soloDances,
          value: [...prevState.soloDances.value, name]
        }
      }))
    } else {
      this.setState(prevState => ({
        soloDances: {
          ...prevState.soloDances,
          value: prevState.soloDances.value.filter(dance => dance !== name)
        }
      }))
    }
  }

  addEntry(event) {
    event.preventDefault();
    if (this.getErrorsCount() !== 0) {
      return false;
    }

    const {addEntry} = this.props;
    const {feis, student, soloDances} = this.state;
    const soloDancesObjects = soloDances.value.map(soloDance => {
      const lastSpaceIndex = soloDance.lastIndexOf(' ');
      const danceName = soloDance.slice(0, lastSpaceIndex);
      const level = soloDance.slice(lastSpaceIndex + 1);
      return {danceName, level};
    });

    const body = {
      feisId: feis.value.id,
      studentId: student.value.id,
      soloDances: soloDancesObjects
    };

    addEntry(body);
    this.close();
  }

  editEntry(event) {
    event.preventDefault();
    if (this.getErrorsCount() !== 0) {
      return false;
    }

    const {editEntry, entry} = this.props;
    const {feis, student, soloDances} = this.state;
    const soloDancesObjects = soloDances.value.map(soloDance => {
      const lastSpaceIndex = soloDance.lastIndexOf(' ');
      const danceName = soloDance.slice(0, lastSpaceIndex);
      const level = soloDance.slice(lastSpaceIndex + 1);
      return {danceName, level};
    });

    const body = {
      feisId: feis.value.id,
      studentId: student.value.id,
      soloDances: soloDancesObjects
    };
    editEntry(entry.id, body);
    this.close();
  }

  close() {
    this.props.onClose();
    this.setState({
      entryError: false,
      student: {
        value: null,
        errorText: '',
        checkers: [exists]
      },
      studentName: '',
      feis: {
        value: null,
        errorText: '',
        checkers: [exists]
      },
      feisName: '',
      soloDances: {
        value: [],
        errorText: '',
        checkers: [notEmpty]
      }
    });
  }

  getErrorsCount() {
    let errorsCount = 0;
    for (let property in this.state) {
      if (!this.state[property].hasOwnProperty('checkers')) continue;

      const {value} = this.state[property];
      const errorText = this.check(property, value);
      if (errorText) {
        errorsCount++;
        this.setState({
          [property]: {
            ...this.state[property],
            errorText
          }
        })
      } else {
        this.setState({
          [property]: {
            ...this.state[property],
            errorText: ''
          }
        })
      }
    }
    return errorsCount;
  };

  check(property, value) {
    const {checkers} = this.state[property];
    for (let checker of checkers) {
      const errorText = checker(value);
      if (errorText) {
        return errorText
      }
    }
    return '';
  }

  render() {
    const {open, entry, feises, students} = this.props;
    const {feis, feisName, student, studentName, soloDances} = this.state;

    const hasErrorText = formControlValue => {
      return formControlValue.errorText !== '' &&
        formControlValue.errorText !== undefined &&
        formControlValue.errorText !== null
    };

    const createFeisItem = feis => {
      return <MenuItem key={feis.id} value={feis.name}>{feis.name}</MenuItem>
    };
    const feisItems = feises.map(createFeisItem);

    const createStudentItem = student => {
      return <MenuItem key={student.id} value={`${student.surname} ${student.firstName}`}>
        {student.surname} {student.firstName}
      </MenuItem>
    };
    const studentItems = students.map(createStudentItem);

    const selectStyle = {
      minWidth: '200px'
    };

    const form = <Dialog open={open} onClose={this.close}>
      <DialogTitle>{entry ? <Translate value="entryEditing"/> : <Translate value="entryAddition"/>}</DialogTitle>
      <DialogContent>
        {entry ?
          <Typography>{feisName}</Typography> :
          <FormControl error={hasErrorText(feis)} required fullWidth>
            <InputLabel id="feis-label"><Translate value="nameOfFeis"/></InputLabel>
            <Select
              style={selectStyle}
              value={feisName}
              labelId="feis-label"
              name="feis"
              onChange={this.setFeis}
              displayEmpty>
              {feisItems}
            </Select>
            {hasErrorText(feis) ? <FormHelperText>{feis.errorText}</FormHelperText> : null}
          </FormControl>}
        {entry ?
          <Typography>{studentName}</Typography> :
          <FormControl error={hasErrorText(student)} required fullWidth>
            <InputLabel id="student-label"><Translate value="nameOfStudent"/></InputLabel>
            <Select
              style={selectStyle}
              value={studentName}
              labelId="student-label"
              name="student"
              onChange={this.setStudent}
              displayEmpty>
              {studentItems}
            </Select>
            {hasErrorText(student) ? <FormHelperText>{student.errorText}</FormHelperText> : null}
          </FormControl>}
        {student.value ?
          <DanceControls
            setDance={this.setDance}
            student={student.value}
            soloDances={entry ? soloDances.value : null}/>
          : null}
        {student.value && hasErrorText(soloDances) ?
          <FormHelperText error>{soloDances.errorText}</FormHelperText> : null}
      </DialogContent>
      <DialogActions>
        <Button onClick={entry ? this.editEntry : this.addEntry} color="primary">
          <Translate value="save"/>
        </Button>
        <Button onClick={this.close} color="primary">
          <Translate value="cancel"/>
        </Button>
      </DialogActions>
    </Dialog>;
    return (
      <ThemeProvider theme={theme}>{form}</ThemeProvider>
    )
  }
}

EntryForm.propTypes = {
  open: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  entry: PropTypes.object
};

const mapStateToProps = state => ({
  error: state.teacherEntriesReducer.error,
  students: state.studentsReducer.students,
  feises: state.teacherFeisesReducer.feises
});

const mapDispatchToProps = {
  getStudents,
  getFeises,
  addEntry,
  editEntry
};

export default connect(mapStateToProps, mapDispatchToProps)(EntryForm);
