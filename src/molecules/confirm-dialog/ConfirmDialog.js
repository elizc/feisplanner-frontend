import React from 'react';
import PropTypes from 'prop-types';
import {Button, Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions} from "@material-ui/core";
import ThemeProvider from "@material-ui/styles/ThemeProvider";
import theme from "../../utils/theme";
import {Translate} from "react-redux-i18n";

const ConfirmDialog = ({open, onClose, onConfirm, title, content, confirmText, cancelText, options}) => {
  const handleConfirm =() => {
    onClose();
    onConfirm(options);
  };

  const dialog = <Dialog
    open={open}
    onClose={onClose}>
    <DialogTitle><Translate value={title}/></DialogTitle>
    <DialogContent>
      {content ?
        <DialogContentText><Translate value={content}/></DialogContentText> :
        null}
    </DialogContent>
    <DialogActions>
      <Button onClick={handleConfirm} color="primary">
        {<Translate value={confirmText}/> || 'OK'}
      </Button>
      <Button onClick={onClose} color="primary" autoFocus>
        {<Translate value={cancelText}/> || 'Cancel'}
      </Button>
    </DialogActions>
  </Dialog>;

  return (
    <ThemeProvider theme={theme}>{dialog}</ThemeProvider>
  );
};

ConfirmDialog.propTypes = {
  open: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  onConfirm: PropTypes.func.isRequired,
  title: PropTypes.string,
  content: PropTypes.string,
  confirmText: PropTypes.string,
  cancelText: PropTypes.string,
  options: PropTypes.object,
};

export default ConfirmDialog;
