import React from 'react';
import PropTypes from 'prop-types';
import {Drawer, makeStyles} from "@material-ui/core";
import ThemeProvider from "@material-ui/styles/ThemeProvider";
import * as jwt from "jsonwebtoken";
import AdminMenu from "../../atoms/admin-menu/AdminMenu";
import TeacherMenu from "../../atoms/teacher-menu/TeacherMenu";
import theme from "../../utils/theme";
import {constants, roles} from "../../utils/constants";

const SidePanel = ({open, onMobileClose, variant}) => {
  const useStyles = makeStyles(theme => ({
    toolbar: theme.mixins.toolbar
  }));
  const classes = useStyles();

  const token = localStorage.getItem('token');
  const parsedToken = jwt.verify(token, constants.JWT_KEY);
  const userRole = parsedToken[constants.USER_ROLE_KEY];

  const drawer =
    <Drawer
      variant={variant}
      open={open}
      onClose={onMobileClose}
      ModalProps={{
        keepMounted: true,
      }}>
      <div className={classes.toolbar}/>
      {userRole === roles.ROLE_ADMIN ? <AdminMenu/> : <TeacherMenu/>}
    </Drawer>;
  return (
    <ThemeProvider theme={theme}>{drawer}</ThemeProvider>
  )
};

SidePanel.propTypes = {
  open: PropTypes.bool,
  onMobileClose: PropTypes.func,
  variant: PropTypes.string.isRequired,
};

export default SidePanel;
