import React, {Component} from 'react';
import {connect} from "react-redux";
import PropTypes from 'prop-types';
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  FormControl,
  FormHelperText,
  InputLabel,
  MenuItem,
  Select,
  TextField
} from "@material-ui/core";
import ThemeProvider from "@material-ui/styles/ThemeProvider";
import {Translate} from 'react-redux-i18n';
import theme from "../../utils/theme";
import {containsLatinLetters, exists, notContainsNationalChars, notContainsNumbers} from "../../utils/formCheckers";
import {isEqual} from "../../utils/comparator";
import {makeFirstLettersBig} from "../../utils/stringFunctions";
import {addSchool, editSchool} from "../../organisms/schools-table/actions";

export class SchoolForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      schoolError: false,
      name: {
        value: '',
        errorText: '',
        checkers: [exists, containsLatinLetters, notContainsNationalChars, notContainsNumbers]
      },
      region: {
        value: '',
        errorText: '',
        checkers: [exists]
      },
      city: {
        value: '',
        errorText: '',
        checkers: [exists, containsLatinLetters, notContainsNationalChars, notContainsNumbers]
      },
    };
    this.setStateProperty = this.setStateProperty.bind(this);
    this.editSchool = this.editSchool.bind(this);
    this.addSchool = this.addSchool.bind(this);
    this.close = this.close.bind(this);
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevProps.school !== this.props.school) {
      const {name, region, city} = prevState;
      const {school} = this.props;
      this.setState({
        name: {
          ...name,
          value: school ? school.name : ''
        },
        region: {
          ...region,
          value: school ? school.region : ''
        },
        city: {
          ...city,
          value: school ? school.city : ''
        },
      })
    }

    if (!isEqual(prevProps, this.props) && this.props.error) {
      this.setState({
        schoolError: true
      });
    }
  }


  setStateProperty(event) {
    const {target} = event;
    const property = target.name;
    const value = target.value;

    this.setState(prevState => ({
      schoolError: false,
      [property]: {
        ...prevState[property],
        value
      }
    }));
  }

  editSchool(event) {
    event.preventDefault();
    if (this.getErrorsCount() !== 0) {
      return false;
    }

    const {name, region, city} = this.state;
    const {school, editSchool} = this.props;
    const body = {
      id: school.id,
      name: makeFirstLettersBig(name.value),
      region: region.value,
      city: makeFirstLettersBig(city.value)
    };
    editSchool(body);
    this.close();
  }

  addSchool(event) {
    event.preventDefault();
    if (this.getErrorsCount() !== 0) {
      return false;
    }

    const {name, region, city} = this.state;
    const {addSchool} = this.props;
    const body = {
      name: makeFirstLettersBig(name.value),
      region: region.value,
      city: makeFirstLettersBig(city.value)
    };
    addSchool(body);
    this.close();
  }

  close() {
    this.props.onClose();
    this.setState({
      schoolError: false,
      name: {
        value: '',
        errorText: '',
        checkers: [exists, containsLatinLetters, notContainsNationalChars, notContainsNumbers]
      },
      region: {
        value: '',
        errorText: '',
        checkers: [exists]
      },
      city: {
        value: '',
        errorText: '',
        checkers: [exists, containsLatinLetters, notContainsNationalChars, notContainsNumbers]
      },
    })
  }

  getErrorsCount() {
    let errorsCount = 0;
    for (let property in this.state) {
      if (!this.state[property].hasOwnProperty('checkers')) continue;

      const {value} = this.state[property];
      const errorText = this.check(property, value);
      if (errorText) {
        errorsCount++;
        this.setState({
          [property]: {
            ...this.state[property],
            errorText
          }
        })
      } else {
        this.setState({
          [property]: {
            ...this.state[property],
            errorText: ''
          }
        })
      }
    }
    return errorsCount;
  };

  check(property, value) {
    const {checkers} = this.state[property];
    for (let checker of checkers) {
      const errorText = checker(value);
      if (errorText) {
        return errorText
      }
    }
    return '';
  }

  render() {
    const {open, school} = this.props;
    const {name, region, city, schoolError} = this.state;
    const hasErrorText = formControlValue => {
      return formControlValue.errorText !== '' &&
        formControlValue.errorText !== undefined &&
        formControlValue.errorText !== null
    };

    const regionsNames = [
      'Britain',
      'Eastern Europe',
      'Ireland',
      'North America',
      'Other',
      'Siberia',
      'Western Europe'
    ];
    const createSelectItems = regionName => {
      return <MenuItem value={regionName} key={regionName}>{regionName}</MenuItem>
    };
    const selectItems = regionsNames.map(createSelectItems);

    const form = <Dialog open={open} onClose={this.close}>
      <DialogTitle>
        {school ?
          <Translate value="schoolEditing"/> :
          <Translate value="schoolAddition"/>}
      </DialogTitle>
      <DialogContent>
        <TextField
          label={<Translate value="nameOfSchool"/>}
          name="name"
          value={name.value}
          margin="dense"
          onChange={this.setStateProperty}
          error={hasErrorText(name)}
          helperText={hasErrorText(name) ? name.errorText : ''}
          required
          fullWidth/>
        <FormControl error={hasErrorText(region)} required fullWidth>
          <InputLabel id="region-label"><Translate value="region"/></InputLabel>
          <Select
            labelId="region-label"
            name="region"
            value={region.value}
            onChange={this.setStateProperty}
            displayEmpty>
            {selectItems}
          </Select>
          {hasErrorText(region) ? <FormHelperText>{region.errorText}</FormHelperText> : null}
        </FormControl>
        <TextField
          label={<Translate value="city"/>}
          name="city"
          value={city.value}
          margin="dense"
          onChange={this.setStateProperty}
          error={hasErrorText(city)}
          helperText={hasErrorText(city) ? city.errorText : ''}
          required
          fullWidth/>
        {schoolError ?
          <FormHelperText error>
            <Translate value="errorText.unableToUpdateSchools"/>
          </FormHelperText> :
          null}
      </DialogContent>
      <DialogActions>
        <Button onClick={school ? this.editSchool : this.addSchool} color="primary">
          <Translate value="save"/>
        </Button>
        <Button onClick={this.close} color="primary">
          <Translate value="cancel"/>
        </Button>
      </DialogActions>
    </Dialog>;
    return (
      <ThemeProvider theme={theme}>{form}</ThemeProvider>
    );
  }
}

SchoolForm.propTypes = {
  open: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  school: PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    region: PropTypes.string.isRequired,
    city: PropTypes.string.isRequired,
  }),
};

const mapStateToProps = state => ({
  error: state.schoolsReducer.errorRegister
});

const mapDispatchToProps = {
  editSchool,
  addSchool
};

export default connect(mapStateToProps, mapDispatchToProps)(SchoolForm);
