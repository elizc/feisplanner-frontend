import React, {Component} from 'react';
import PropTypes from "prop-types";
import {connect} from "react-redux";
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  FormControl,
  FormControlLabel,
  FormGroup,
  FormHelperText,
  InputLabel,
  MenuItem,
  Radio,
  RadioGroup,
  Select,
  TextField,
  Typography
} from "@material-ui/core";
import {Translate} from "react-redux-i18n";
import ThemeProvider from "@material-ui/styles/ThemeProvider";
import * as jwt from "jsonwebtoken";
import theme from "../../utils/theme";
import {containsLatinLetters, exists, notContainsNationalChars, notContainsNumbers} from "../../utils/formCheckers";
import {makeFirstLettersBig} from "../../utils/stringFunctions";
import {isEqual} from "../../utils/comparator";
import {constants} from "../../utils/constants";
import {addStudent, editStudent} from "../../organisms/students-table/actions";

class StudentForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      studentError: false,
      firstName: {
        value: '',
        errorText: '',
        checkers: [exists, containsLatinLetters, notContainsNationalChars, notContainsNumbers]
      },
      surname: {
        value: '',
        errorText: '',
        checkers: [exists, containsLatinLetters, notContainsNationalChars, notContainsNumbers]
      },
      birthDay: {
        value: '',
        errorText: '',
        checkers: [exists]
      },
      birthMonth: {
        value: '',
        errorText: '',
        checkers: [exists]
      },
      birthYear: {
        value: '',
        errorText: '',
        checkers: [exists]
      },
      gender: {
        value: 'Female',
        errorText: '',
        checkers: [exists]
      },
      reelLevel: {
        value: 'Beginner',
        errorText: '',
        checkers: [exists]
      },
      lightLevel: {
        value: 'Beginner',
        errorText: '',
        checkers: [exists]
      },
      slipLevel: {
        value: 'Beginner',
        errorText: '',
        checkers: [exists]
      },
      singleLevel: {
        value: 'Beginner',
        errorText: '',
        checkers: [exists]
      },
      trebleLevel: {
        value: 'Beginner',
        errorText: '',
        checkers: [exists]
      },
      hornpipeLevel: {
        value: 'Beginner',
        errorText: '',
        checkers: [exists]
      },
      tradSetLevel: {
        value: 'Beginner',
        errorText: '',
        checkers: [exists]
      },
      wonPrelimsCount: {
        value: '0',
        errorText: '',
        checkers: [exists]
      },
    };
    this.setStateProperty = this.setStateProperty.bind(this);
    this.addStudent = this.addStudent.bind(this);
    this.editStudent = this.editStudent.bind(this);
    this.close = this.close.bind(this);
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevProps.student !== this.props.student) {
      const {
        firstName, surname, gender, birthDay, birthMonth, birthYear,
        reelLevel, lightLevel, slipLevel, singleLevel, trebleLevel,
        hornpipeLevel, tradSetLevel, wonPrelimsCount
      } = prevState;
      const {student} = this.props;
      this.setState({
        firstName: {
          ...firstName,
          value: student ? student.firstName : ''
        },
        surname: {
          ...surname,
          value: student ? student.surname : ''
        },
        gender: {
          ...gender,
          value: student ? student.gender : 'Female'
        },
        birthDay: {
          ...birthDay,
          value: student ? student.birthDate.slice(3, 5) : ''
        },
        birthMonth: {
          ...birthMonth,
          value: student ? student.birthDate.slice(0, 2) : ''
        },
        birthYear: {
          ...birthYear,
          value: student ? student.birthDate.slice(6) : ''
        },
        reelLevel: {
          ...reelLevel,
          value: student ? student.reelLevel : 'Beginner'
        },
        lightLevel: {
          ...lightLevel,
          value: student ? student.lightLevel : 'Beginner'
        },
        slipLevel: {
          ...slipLevel,
          value: student ? student.slipLevel : 'Beginner'
        },
        singleLevel: {
          ...singleLevel,
          value: student ? student.singleLevel : 'Beginner'
        },
        trebleLevel: {
          ...trebleLevel,
          value: student ? student.trebleLevel : 'Beginner'
        },
        hornpipeLevel: {
          ...hornpipeLevel,
          value: student ? student.hornpipeLevel : 'Beginner'
        },
        tradSetLevel: {
          ...tradSetLevel,
          value: student ? student.tradSetLevel : 'Beginner'
        },
        wonPrelimsCount: {
          ...wonPrelimsCount,
          value: student ? student.wonPrelimsCount.toString() : '0'
        },
      })
    }


    if (!isEqual(prevProps, this.props) && this.props.error) {
      this.setState({
        studentError: true
      });
    }
  }

  setStateProperty(event) {
    const {target} = event;
    const property = target.name;
    const value = target.value;

    this.setState(prevState => ({
      studentError: false,
      [property]: {
        ...prevState[property],
        value
      }
    }));
  }

  addStudent(event) {
    event.preventDefault();
    if (this.getErrorsCount() !== 0) {
      return false;
    }

    const {
      firstName, surname, gender, birthDay, birthMonth, birthYear,
      reelLevel, lightLevel, slipLevel, singleLevel, trebleLevel,
      hornpipeLevel, tradSetLevel, wonPrelimsCount
    } = this.state;
    const {addStudent} = this.props;

    const token = localStorage.getItem('token');
    const parsedToken = jwt.verify(token, constants.JWT_KEY);
    const userId = parsedToken[constants.USER_ID_KEY];

    const body = {
      firstName: makeFirstLettersBig(firstName.value),
      surname: makeFirstLettersBig(surname.value),
      gender: gender.value,
      birthDate: `${birthMonth.value}-${birthDay.value}-${birthYear.value}`,
      reelLevel: reelLevel.value,
      lightLevel: lightLevel.value,
      slipLevel: slipLevel.value,
      singleLevel: singleLevel.value,
      trebleLevel: trebleLevel.value,
      hornpipeLevel: hornpipeLevel.value,
      tradSetLevel: tradSetLevel.value,
      wonPrelimsCount: +wonPrelimsCount.value,
      userId
    };
    addStudent(body);
    this.close();
  }

  editStudent(event) {
    event.preventDefault();
    if (this.getErrorsCount() !== 0) {
      return false;
    }

    const {
      firstName, surname, gender, birthDay, birthMonth, birthYear,
      reelLevel, lightLevel, slipLevel, singleLevel, trebleLevel,
      hornpipeLevel, tradSetLevel, wonPrelimsCount
    } = this.state;
    const {student, editStudent} = this.props;
    const body = {
      id: student.id,
      firstName: makeFirstLettersBig(firstName.value),
      surname: makeFirstLettersBig(surname.value),
      gender: gender.value,
      birthDate: `${birthMonth.value}-${birthDay.value}-${birthYear.value}`,
      reelLevel: reelLevel.value,
      lightLevel: lightLevel.value,
      slipLevel: slipLevel.value,
      singleLevel: singleLevel.value,
      trebleLevel: trebleLevel.value,
      hornpipeLevel: hornpipeLevel.value,
      tradSetLevel: tradSetLevel.value,
      wonPrelimsCount: +wonPrelimsCount.value,
    };
    editStudent(body);
    this.close();
  }

  getErrorsCount() {
    let errorsCount = 0;
    for (let property in this.state) {
      if (!this.state[property].hasOwnProperty('checkers')) continue;

      const {value} = this.state[property];
      const errorText = this.check(property, value);
      if (errorText) {
        errorsCount++;
        this.setState({
          [property]: {
            ...this.state[property],
            errorText
          }
        })
      } else {
        this.setState({
          [property]: {
            ...this.state[property],
            errorText: ''
          }
        })
      }
    }
    return errorsCount;
  };

  close() {
    this.props.onClose();
    this.setState({
      studentError: false,
      firstName: {
        value: '',
        errorText: '',
        checkers: [exists, containsLatinLetters, notContainsNationalChars, notContainsNumbers]
      },
      surname: {
        value: '',
        errorText: '',
        checkers: [exists, containsLatinLetters, notContainsNationalChars, notContainsNumbers]
      },
      birthDay: {
        value: '',
        errorText: '',
        checkers: [exists]
      },
      birthMonth: {
        value: '',
        errorText: '',
        checkers: [exists]
      },
      birthYear: {
        value: '',
        errorText: '',
        checkers: [exists]
      },
      gender: {
        value: 'Female',
        errorText: '',
        checkers: [exists]
      },
      reelLevel: {
        value: 'Beginner',
        errorText: '',
        checkers: [exists]
      },
      lightLevel: {
        value: 'Beginner',
        errorText: '',
        checkers: [exists]
      },
      slipLevel: {
        value: 'Beginner',
        errorText: '',
        checkers: [exists]
      },
      singleLevel: {
        value: 'Beginner',
        errorText: '',
        checkers: [exists]
      },
      trebleLevel: {
        value: 'Beginner',
        errorText: '',
        checkers: [exists]
      },
      hornpipeLevel: {
        value: 'Beginner',
        errorText: '',
        checkers: [exists]
      },
      tradSetLevel: {
        value: 'Beginner',
        errorText: '',
        checkers: [exists]
      },
      wonPrelimsCount: {
        value: '0',
        errorText: '',
        checkers: [exists]
      }
    });
  }

  check(property, value) {
    const {checkers} = this.state[property];
    for (let checker of checkers) {
      const errorText = checker(value);
      if (errorText) {
        return errorText
      }
    }
    return '';
  }

  render() {
    const {open, student} = this.props;
    const {
      firstName, surname, gender, birthDay, birthMonth, birthYear,
      reelLevel, lightLevel, slipLevel, singleLevel, trebleLevel,
      hornpipeLevel, tradSetLevel, wonPrelimsCount, studentError
    } = this.state;

    const hasErrorText = formControlValue => {
      return formControlValue.errorText !== '' &&
        formControlValue.errorText !== undefined &&
        formControlValue.errorText !== null
    };

    const createSelectItem = ({text, value}) => {
      return <MenuItem value={value} key={text}>
        <Translate value={text}/>
      </MenuItem>
    };

    const months = [
      {text: 'months.january', value: '01'},
      {text: 'months.february', value: '02'},
      {text: 'months.march', value: '03'},
      {text: 'months.april', value: '04'},
      {text: 'months.may', value: '05'},
      {text: 'months.june', value: '06'},
      {text: 'months.july', value: '07'},
      {text: 'months.august', value: '08'},
      {text: 'months.september', value: '09'},
      {text: 'months.october', value: '10'},
      {text: 'months.november', value: '11'},
      {text: 'months.december', value: '12'}
    ];
    const monthsItems = months.map(createSelectItem);

    const dayItems = [];
    for (let day = 1; day <= 31; day++) {
      const dayValue = day < 10 ? '0' + day : day.toString();
      dayItems.push(<MenuItem value={dayValue} key={day}>{day}</MenuItem>)
    }

    const yearItems = [];
    const currentYear = new Date().getFullYear();
    for (let year = currentYear - 1; year >= 1960; year--) {
      yearItems.push(<MenuItem value={year} key={year}>{year}</MenuItem>);
    }

    const formGroupStyle = {
      marginTop: '20px'
    };

    const createDanceGroup = (danceName, groupName, value) => {
      const levels = ['Beginner', 'Primary', 'Intermediate', 'Open'];
      const createRadio = level => {
        return <FormControlLabel
          key={level}
          value={level}
          control={<Radio color="primary"/>}
          label={level}/>
      };
      const radios = levels.map(createRadio);
      return <FormControl style={formGroupStyle}>
        <Typography>{danceName}</Typography>
        <RadioGroup name={groupName} value={value} onChange={this.setStateProperty} row>
          {radios}
        </RadioGroup>
      </FormControl>
    };

    const form = <Dialog open={open} onClose={this.close}>
      <DialogTitle>
        {student ?
          <Translate value="studentEditing"/> :
          <Translate value="studentAddition"/>}
      </DialogTitle>
      <DialogContent>
        <TextField
          label={<Translate value="firstName"/>}
          name="firstName"
          value={firstName.value}
          margin="dense"
          onChange={this.setStateProperty}
          error={hasErrorText(firstName)}
          helperText={hasErrorText(firstName) ? firstName.errorText : ''}
          required
          fullWidth/>
        <TextField
          label={<Translate value="surname"/>}
          name="surname"
          value={surname.value}
          margin="dense"
          onChange={this.setStateProperty}
          error={hasErrorText(surname)}
          helperText={hasErrorText(surname) ? surname.errorText : ''}
          required
          fullWidth/>
        <FormControl style={formGroupStyle}>
          <Typography>Gender</Typography>
          <RadioGroup name="gender" value={gender.value} onChange={this.setStateProperty} row>
            <FormControlLabel
              value="Female"
              control={<Radio color="primary"/>}
              label={<Translate value="female"/>}/>
            <FormControlLabel
              value="Male"
              control={<Radio color="primary"/>}
              label={<Translate value="male"/>}/>
          </RadioGroup>
        </FormControl>
        <FormGroup style={formGroupStyle}>
          <Typography>
            <Translate value="dateOfBirth"/>
          </Typography>
          <FormControl error={hasErrorText(birthDay)} required>
            <InputLabel id="birth-day-label">
              <Translate value="day"/>
            </InputLabel>
            <Select
              labelId="birth-day-label"
              name="birthDay"
              value={birthDay.value}
              onChange={this.setStateProperty}
              displayEmpty>
              {dayItems}
            </Select>
            {hasErrorText(birthDay) ? <FormHelperText>{birthDay.errorText}</FormHelperText> : null}
          </FormControl>
          <FormControl error={hasErrorText(birthMonth)} required>
            <InputLabel id="birth-month-label">
              <Translate value="month"/>
            </InputLabel>
            <Select
              labelId="birth-month-label"
              name="birthMonth"
              value={birthMonth.value}
              onChange={this.setStateProperty}
              displayEmpty>
              {monthsItems}
            </Select>
            {hasErrorText(birthMonth) ? <FormHelperText>{birthMonth.errorText}</FormHelperText> : null}
          </FormControl>
          <FormControl error={hasErrorText(birthYear)} required>
            <InputLabel id="birth-year-label">
              <Translate value="year"/>
            </InputLabel>
            <Select
              labelId="birth-year-label"
              name="birthYear"
              value={birthYear.value}
              onChange={this.setStateProperty}
              displayEmpty>
              {yearItems}
            </Select>
            {hasErrorText(birthYear) ? <FormHelperText>{birthYear.errorText}</FormHelperText> : null}
          </FormControl>
        </FormGroup>
        {createDanceGroup('Reel', 'reelLevel', reelLevel.value)}
        {createDanceGroup('Light Jig', 'lightLevel', lightLevel.value)}
        {createDanceGroup('Slip Jig', 'slipLevel', slipLevel.value)}
        {createDanceGroup('Single Jig', 'singleLevel', singleLevel.value)}
        {createDanceGroup('Treble Jig', 'trebleLevel', trebleLevel.value)}
        {createDanceGroup('Hornpipe', 'hornpipeLevel', hornpipeLevel.value)}
        {createDanceGroup('Trad Set', 'tradSetLevel', tradSetLevel.value)}
        <FormControl style={formGroupStyle}>
          <Typography><Translate value="countOfWonPrelims"/></Typography>
          <RadioGroup name="wonPrelimsCount" value={wonPrelimsCount.value} onChange={this.setStateProperty} row>
            <FormControlLabel value="0" control={<Radio color="primary"/>} label="0"/>
            <FormControlLabel value="1" control={<Radio color="primary"/>} label="1"/>
            <FormControlLabel value="2" control={<Radio color="primary"/>} label="2"/>
          </RadioGroup>
        </FormControl>
        {studentError ?
          <FormHelperText error>
            <Translate value="errorText.unableToUpdateStudents"/>
          </FormHelperText> :
          null}
      </DialogContent>
      <DialogActions>
        <Button color="primary" onClick={student ? this.editStudent : this.addStudent}>
          <Translate value="save"/>
        </Button>
        <Button onClick={this.close} color="primary">
          <Translate value="cancel"/>
        </Button>
      </DialogActions>
    </Dialog>;
    return (
      <ThemeProvider theme={theme}>{form}</ThemeProvider>
    );
  }
}

StudentForm.propTypes = {
  open: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  student: PropTypes.shape({
    id: PropTypes.string.isRequired,
    firstName: PropTypes.string.isRequired,
    surname: PropTypes.string.isRequired,
    gender: PropTypes.string.isRequired,
    birthDate: PropTypes.string.isRequired,
    reelLevel: PropTypes.string.isRequired,
    lightLevel: PropTypes.string.isRequired,
    slipLevel: PropTypes.string.isRequired,
    singleLevel: PropTypes.string.isRequired,
    trebleLevel: PropTypes.string.isRequired,
    hornpipeLevel: PropTypes.string.isRequired,
    tradSetLevel: PropTypes.string.isRequired,
    wonPrelimsCount: PropTypes.number.isRequired,
  }),
};

const mapStateToProps = state => ({
  error: state.studentsReducer.error
});

const mapDispatchToProps = {
  addStudent,
  editStudent
};

export default connect(mapStateToProps, mapDispatchToProps)(StudentForm);
