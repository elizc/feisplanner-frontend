import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Grid, TextField, Typography} from "@material-ui/core";
import ThemeProvider from "@material-ui/styles/ThemeProvider";
import {Translate} from "react-redux-i18n";
import theme from "../../utils/theme";
import './SearchForm.css';

class SearchForm extends Component {
  constructor(props) {
    super(props);
    const {fieldsData} = this.props;
    this.state = {};
    for (let field of fieldsData) {
      this.state[field.name] = '';
    }
    this.search = this.search.bind(this);
  }

  search(event) {
    const {target} = event;
    const property = target.name;
    const value = target.value;

    this.props.search({...this.state, [property]: value});

    this.setState({
      [property]: value
    })
  }

  render() {
    const {fieldsData} = this.props;
    const createField = field => {
      const style = {
        marginRight: '20px',
        backgroundColor: '#fff',
      };
      return <Grid item xs={12} md={4} key={field.name}>
        <TextField
        label={<Translate value={field.label}/>}
        name={field.name}
        margin="dense"
        variant="outlined"
        style={style}
        className="search-form__field"
        onChange={this.search}
        fullWidth/>
      </Grid>
    };
    const fields = fieldsData.map(createField);

    const form = <form className="search-form">
      <Typography variant="subtitle2">
        <Translate value="search"/>
      </Typography>
      <Grid container spacing={2}>
        {fields}
      </Grid>
    </form>;
    return (
      <ThemeProvider theme={theme}>{form}</ThemeProvider>
    );
  }
}

SearchForm.propTypes = {
  fieldsData: PropTypes.arrayOf(PropTypes.shape({
    name: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
  }).isRequired).isRequired,
  search: PropTypes.func.isRequired,
};

export default SearchForm;
