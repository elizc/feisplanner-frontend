import React, {Component} from 'react';
import Flag from "../../atoms/flag/Flag";
import LangItem from "../../atoms/lang-item/LangItem";
import Language from "../../utils/Language";
import enFlag from '../../atoms/flag/image/en_flag_icon.svg';
import deFlag from '../../atoms/flag/image/de_flag_icon.svg';
import ruFlag from '../../atoms/flag/image/ru_flag_icon.svg';
import store from '../../store';
import {setLocale} from "react-redux-i18n";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {Menu} from "@material-ui/core";
import './LangSelect.css';

class LangSelect extends Component {
  constructor(props) {
    super(props);
    this.state = {
      lang: this.props.lang,
      langListOpen: false,
      anchorEl: null
    };
    this.toggleLangItems = this.toggleLangItems.bind(this);
    this.switchLang = this.switchLang.bind(this);
  }

  toggleLangItems(event) {
    event.persist()
    this.setState(prevState => ({
      langListOpen: !prevState.langListOpen,
      anchorEl: event.target
    }))
  }

  switchLang(lang) {
    store.dispatch(setLocale(lang));
    this.setState(prevState => ({
      lang,
      langListOpen: !prevState.langListOpen
    }))
  }

  render() {
    const {lang, langListOpen, anchorEl} = this.state;
    return (
      <div className="lang-select">
        <Flag langCode={lang} onClick={this.toggleLangItems}/>
        <Menu anchorEl={anchorEl} open={langListOpen} onClose={this.toggleLangItems}>
          <LangItem lang={Language.ENGLISH} flag={enFlag} onClick={this.switchLang}/>
          <LangItem lang={Language.GERMAN} flag={deFlag} onClick={this.switchLang}/>
          <LangItem lang={Language.RUSSIAN} flag={ruFlag} onClick={this.switchLang}/>
        </Menu>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  lang: state.i18n.locale
});

const mapDispatchToProps = dispatch => {
  const actions = {
    setLocale
  };
  return {
    actions: bindActionCreators(actions, dispatch)
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(LangSelect);
