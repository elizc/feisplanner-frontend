import React, {Component} from 'react';
import {connect} from "react-redux";
import PropTypes from "prop-types";
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  FormControl,
  FormGroup,
  FormHelperText,
  InputLabel,
  MenuItem,
  Select,
  TextField,
  Typography
} from "@material-ui/core";
import ThemeProvider from "@material-ui/styles/ThemeProvider";
import {Translate} from "react-redux-i18n";
import {containsLatinLetters, exists, notContainsNationalChars} from "../../utils/formCheckers";
import {isEqual} from "../../utils/comparator";
import {makeFirstLettersBig} from "../../utils/stringFunctions";
import theme from "../../utils/theme";
import {addFeis, editFeis} from "../../organisms/admin-feises-table/actions";
import {getSchools} from "../../organisms/schools-table/actions";

class FeisForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      feisError: false,
      name: {
        value: '',
        errorText: '',
        checkers: [exists, containsLatinLetters, notContainsNationalChars]
      },
      day: {
        value: '',
        errorText: '',
        checkers: [exists]
      },
      month: {
        value: '',
        errorText: '',
        checkers: [exists]
      },
      year: {
        value: '',
        errorText: '',
        checkers: [exists]
      },
      school: {
        value: '',
        errorText: '',
        checkers: [exists]
      }
    };
    this.setStateProperty = this.setStateProperty.bind(this);
    this.addFeis = this.addFeis.bind(this);
    this.editFeis = this.editFeis.bind(this);
    this.close = this.close.bind(this);
  }

  componentDidMount() {
    const {getSchools} = this.props;
    getSchools();
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevProps.feis !== this.props.feis) {
      const {name, day, month, year, school} = prevState;
      const {feis} = this.props;
      this.setState({
        name: {
          ...name,
          value: feis ? feis.name : ''
        },
        day: {
          ...day,
          value: feis ? feis.startDate.slice(3, 5) : ''
        },
        month: {
          ...month,
          value: feis ? feis.startDate.slice(0, 2) : ''
        },
        year: {
          ...year,
          value: feis ? feis.startDate.slice(6) : ''
        },
        school: {
          ...school,
          value: feis ? feis.school.name : ''
        }
      })
    }

    if (!isEqual(prevProps, this.props) && this.props.error) {
      this.setState({
        feisError: true
      });
    }
  }

  setStateProperty(event) {
    const {target} = event;
    const property = target.name;
    const value = target.value;

    this.setState(prevState => ({
      feisError: false,
      [property]: {
        ...prevState[property],
        value
      }
    }));
  }

  addFeis(event) {
    event.preventDefault();
    if (this.getErrorsCount() !== 0) {
      return false;
    }

    const {name, day, month, year, school} = this.state;
    const {addFeis} = this.props;

    const body = {
      name: makeFirstLettersBig(name.value),
      type: 'Local',
      startDate: `${month.value}-${day.value}-${year.value}`,
      endDate: `${month.value}-${day.value}-${year.value}`,
      schoolName: school.value
    };
    addFeis(body);
    this.close();
  }

  editFeis(event) {
    event.preventDefault();
    if (this.getErrorsCount() !== 0) {
      return false;
    }

    const {name, day, month, year, school} = this.state;
    const {feis, editFeis} = this.props;
    const body = {
      name: makeFirstLettersBig(name.value),
      type: 'Local',
      startDate: `${month.value}-${day.value}-${year.value}`,
      endDate: `${month.value}-${day.value}-${year.value}`,
      schoolName: school.value
    };
    editFeis(feis.id, body);
    this.close();
  }

  close() {
    this.props.onClose();
    this.setState({
      feisError: false,
      name: {
        value: '',
        errorText: '',
        checkers: [exists, containsLatinLetters, notContainsNationalChars]
      },
      day: {
        value: '',
        errorText: '',
        checkers: [exists]
      },
      month: {
        value: '',
        errorText: '',
        checkers: [exists]
      },
      year: {
        value: '',
        errorText: '',
        checkers: [exists]
      },
      school: {
        value: '',
        errorText: '',
        checkers: [exists]
      }
    })
  }

  getErrorsCount() {
    let errorsCount = 0;
    for (let property in this.state) {
      if (!this.state[property].hasOwnProperty('checkers')) continue;

      const {value} = this.state[property];
      const errorText = this.check(property, value);
      if (errorText) {
        errorsCount++;
        this.setState({
          [property]: {
            ...this.state[property],
            errorText
          }
        })
      } else {
        this.setState({
          [property]: {
            ...this.state[property],
            errorText: ''
          }
        })
      }
    }
    return errorsCount;
  };

  check(property, value) {
    const {checkers} = this.state[property];
    for (let checker of checkers) {
      const errorText = checker(value);
      if (errorText) {
        return errorText
      }
    }
    return '';
  }

  render() {
    const {open, feis} = this.props;
    const {name, day, month, year, school, feisError} = this.state;

    const hasErrorText = formControlValue => {
      return formControlValue.errorText !== '' &&
        formControlValue.errorText !== undefined &&
        formControlValue.errorText !== null
    };

    const {schools} = this.props;
    const createSchoolItem = ({name}) => {
      return <MenuItem value={name} key={name}>{name}</MenuItem>
    };
    const schoolsItems = schools.map(createSchoolItem);

    const months = [
      {text: 'months.january', value: '01'},
      {text: 'months.february', value: '02'},
      {text: 'months.march', value: '03'},
      {text: 'months.april', value: '04'},
      {text: 'months.may', value: '05'},
      {text: 'months.june', value: '06'},
      {text: 'months.july', value: '07'},
      {text: 'months.august', value: '08'},
      {text: 'months.september', value: '09'},
      {text: 'months.october', value: '10'},
      {text: 'months.november', value: '11'},
      {text: 'months.december', value: '12'},
    ];
    const createMonthItem = ({text, value}) => {
      return <MenuItem value={value} key={text}>{<Translate value={text}/>}</MenuItem>
    };
    const monthsItems = months.map(createMonthItem);

    const dayItems = [];
    for (let day = 1; day <= 31; day++) {
      const dayValue = day < 10 ? '0' + day : day.toString();
      dayItems.push(<MenuItem value={dayValue} key={day}>{day}</MenuItem>)
    }

    const yearItems = [];
    const currentYear = new Date().getFullYear();
    for (let year = currentYear; year <= currentYear + 5; year++) {
      yearItems.push(<MenuItem value={year} key={year}>{year}</MenuItem>);
    }

    const formGroupStyle = {
      marginTop: '20px'
    };

    const form = <Dialog open={open} onClose={this.close}>
      <DialogTitle>
        {feis ?
          <Translate value="feisEditing"/> :
          <Translate value="feisAddition"/>}
      </DialogTitle>
      <DialogContent>
        <TextField
          label={<Translate value="nameOfFeis"/>}
          name="name"
          value={name.value}
          margin="dense"
          onChange={this.setStateProperty}
          error={hasErrorText(name)}
          helperText={hasErrorText(name) ? name.errorText : ''}
          required
          fullWidth/>
        <FormControl error={hasErrorText(school)} required fullWidth>
          <InputLabel id="school-label"><Translate value="school"/></InputLabel>
          <Select
            labelId="school-label"
            value={school.value}
            name="school"
            onChange={this.setStateProperty}
            displayEmpty>
            {schoolsItems}
          </Select>
          {hasErrorText(school) ? <FormHelperText>{school.errorText}</FormHelperText> : null}
        </FormControl>
        <FormGroup style={formGroupStyle}>
          <Typography>
            <Translate value="date"/>
          </Typography>
          <FormControl error={hasErrorText(day)} required>
            <InputLabel id="day-label">
              <Translate value="day"/>
            </InputLabel>
            <Select
              labelId="day-label"
              name="day"
              value={day.value}
              onChange={this.setStateProperty}
              displayEmpty>
              {dayItems}
            </Select>
            {hasErrorText(day) ? <FormHelperText>{day.errorText}</FormHelperText> : null}
          </FormControl>
          <FormControl error={hasErrorText(month)} required>
            <InputLabel id="month-label">
              <Translate value="month"/>
            </InputLabel>
            <Select
              labelId="month-label"
              name="month"
              value={month.value}
              onChange={this.setStateProperty}
              displayEmpty>
              {monthsItems}
            </Select>
            {hasErrorText(month) ? <FormHelperText>{month.errorText}</FormHelperText> : null}
          </FormControl>
          <FormControl error={hasErrorText(year)} required>
            <InputLabel id="year-label">
              <Translate value="year"/>
            </InputLabel>
            <Select
              labelId="year-label"
              name="year"
              value={year.value}
              onChange={this.setStateProperty}
              displayEmpty>
              {yearItems}
            </Select>
            {hasErrorText(year) ? <FormHelperText>{year.errorText}</FormHelperText> : null}
          </FormControl>
        </FormGroup>
        {feisError ?
          <FormHelperText error>
            <Translate value="errorText.unableToUpdateFeises"/>
          </FormHelperText> :
          null}
      </DialogContent>
      <DialogActions>
        <Button onClick={feis ? this.editFeis : this.addFeis} color="primary">
          <Translate value="save"/>
        </Button>
        <Button onClick={this.close} color="primary">
          <Translate value="cancel"/>
        </Button>
      </DialogActions>
    </Dialog>;

    return (
      <ThemeProvider theme={theme}>{form}</ThemeProvider>
    );
  }
}

FeisForm.propTypes = {
  open: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  feis: PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired,
    startDate: PropTypes.string.isRequired,
    endDate: PropTypes.string.isRequired,
    school: PropTypes.shape({
      id: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
    }).isRequired,
  }),
};

const mapStateToProps = state => ({
  schools: state.schoolsReducer.schools,
  error: state.feisesReducer.error
});

const mapDispatchToProps = {
  getSchools,
  addFeis,
  editFeis
};

export default connect(mapStateToProps, mapDispatchToProps)(FeisForm);
