import {types} from './actions';

const initState = {
  pending: false,
  success: false,
  error: false
};

export default (state = initState, action) => {
  switch (action.type) {
    case types.GET_TIMETABLE_PENDING: {
      return {
        ...state,
        pending: true,
        success: false,
        error: false
      }
    }
    case types.GET_TIMETABLE_SUCCESS: {
      return {
        ...state,
        pending: false,
        success: true,
        error: false
      }
    }
    case types.GET_TIMETABLE_ERROR: {
      return {
        ...state,
        pending: false,
        success: false,
        error: true,
      }
    }
    default: {
      return state
    }
  }
}
