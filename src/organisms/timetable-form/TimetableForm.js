import React, {Component} from "react";
import {connect} from "react-redux";
import {
  Button,
  ButtonGroup,
  FormControl,
  FormHelperText,
  InputLabel,
  MenuItem,
  Paper,
  Select,
  TextField,
  Typography
} from "@material-ui/core";
import ThemeProvider from "@material-ui/styles/ThemeProvider";
import {Translate} from "react-redux-i18n";
import {exists} from "../../utils/formCheckers";
import theme from "../../utils/theme";
import {getTimetable} from "./actions";
import './TimetableForm.css';
import Spinner from "../../atoms/spinner/Spinner";

class TimetableForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      hour: {
        value: '9',
        errorText: '',
        checkers: [exists]
      },
      minute: {
        value: '00',
        errorText: '',
        checkers: [exists]
      },
      halls: [{
        name: {
          value: 'Hall A',
          errorText: '',
          checkers: [exists]
        },
        stagesCount: {
          value: '',
          errorText: '',
          checkers: [exists]
        }
      }]
    }
    this.setStateProperty = this.setStateProperty.bind(this);
    this.addHall = this.addHall.bind(this);
    this.removeLastHall = this.removeLastHall.bind(this);
    this.createTimetable = this.createTimetable.bind(this);
    this.getErrorsCount = this.getErrorsCount.bind(this);
  }

  setStateProperty(event) {
    const {value, name} = event.target;
    if (/.+\d/.test(name)) {
      const index = +name.substr(name.length - 1);
      const propertyName = name.substr(0, name.length - 1);

      const {halls} = this.state;
      halls[index][propertyName].value = value

      this.setState({
        halls
      })
    } else {
      this.setState(prevState => ({
        [name]: {
          ...prevState[name],
          value
        }
      }))
    }
  }

  addHall() {
    const {halls} = this.state;
    const letter = String.fromCharCode('A'.charCodeAt(0) + halls.length);
    halls.push({
      name: {
        value: `Hall ${letter}`,
        errorText: '',
        checkers: [exists]
      },
      stagesCount: {
        value: '',
        errorText: '',
        checkers: [exists]
      }
    });

    this.setState({
      halls: halls
    });
  }

  removeLastHall() {
    const {halls} = this.state;
    halls.pop();

    this.setState({
      halls
    });
  }

  createTimetable(event) {
    event.preventDefault();
    if (this.getErrorsCount() !== 0) {
      return false;
    }

    const {hour, minute, halls} = this.state;
    const {getTimetable} = this.props;

    const body = {
      startHours: +hour.value,
      startMinutes: +minute.value,
      halls: halls.map(item => ({name: item.name.value, stagesCount: item.stagesCount.value}))
    }
    getTimetable(body);
  }

  getErrorsCount() {
    let errorsCount = 0;
    for (let property in this.state) {
      if (property === 'halls') {
        for (let i = 0; i < this.state.halls.length; i++) {
          for (let hallProperty in  this.state.halls[i]) {
            const {value, checkers} = this.state.halls[i][hallProperty];
            const errorText = this.check(checkers, value);
            let {halls} = this.state;
            if (errorText) {
              errorsCount++;
              halls[i][hallProperty] = {
                ...halls[i][hallProperty],
                errorText
              }
            } else {
              halls[i][hallProperty] = {
                ...halls[i][hallProperty],
                errorText: ''
              }
            }

            this.setState({
              halls
            });
          }
        }
      }
      if (!this.state[property].hasOwnProperty('checkers')) continue;

      const {value, checkers} = this.state[property];
      const errorText = this.check(checkers, value);
      if (errorText) {
        errorsCount++;
        this.setState({
          [property]: {
            ...this.state[property],
            errorText
          }
        })
      } else {
        this.setState({
          [property]: {
            ...this.state[property],
            errorText: ''
          }
        })
      }
    }
    return errorsCount;
  };

  check(checkers, value) {
    for (let checker of checkers) {
      const errorText = checker(value);
      if (errorText) {
        return errorText
      }
    }
    return '';
  }

  render() {
    const {halls, hour, minute} = this.state;
    const hasErrorText = formControlValue => {
      return formControlValue.errorText !== '' &&
        formControlValue.errorText !== undefined &&
        formControlValue.errorText !== null
    };

    const stagesItems = [];
    for (let stage = 1; stage <= 3; stage++) {
      stagesItems.push(<MenuItem value={stage} key={stage}>{stage}</MenuItem>)
    }

    let hallItems = [];
    for (let i = 0; i < halls.length; i++) {
      const {name, stagesCount} = halls[i];
      hallItems.push(
        <div className="timetable-form__hall" key={i}>
          <TextField
            value={halls[i].name.value}
            label={<Translate value="nameOfHall"/>}
            name={`name${i}`}
            onChange={this.setStateProperty}
            error={hasErrorText(name)}
            helperText={hasErrorText(name) ? name.errorText : ''}
            margin="dense"
            required
            fullWidth/>
          <FormControl error={hasErrorText(stagesCount)} required fullWidth>
            <InputLabel id="stages-label"><Translate value="countOfStages"/></InputLabel>
            <Select
              value={halls[i].stagesCount.value}
              labelId="stages-label"
              name={`stagesCount${i}`}
              onChange={this.setStateProperty}
              displayEmpty>
              {stagesItems}
            </Select>
            {hasErrorText(stagesCount) ? <FormHelperText>{stagesCount.errorText}</FormHelperText> : null}
          </FormControl>
        </div>
      )
    }

    const hoursItems = [];
    for (let hour = 8; hour <= 20; hour++) {
      hoursItems.push(<MenuItem value={hour.toString()} key={hour}>{hour}</MenuItem>)
    }

    const minutesItems = [];
    for (let minute = 0; minute <= 55; minute += 5) {
      const minuteValue = minute < 10 ? '0' + minute : minute.toString();
      minutesItems.push(<MenuItem value={minuteValue} key={minute}>{minuteValue}</MenuItem>)
    }

    const form = <form className="timetable-form__content">
      {hallItems}
      <div className="timetable-form__halls-buttons">
        <ButtonGroup color="primary" variant="contained">
          <Button onClick={this.removeLastHall} disabled={halls.length <= 1}>-</Button>
          <Button onClick={this.addHall} disabled={halls.length >= 9}>+</Button>
        </ButtonGroup>
      </div>
      <Typography><Translate value="feisStartTime"/></Typography>
      <div className="timetable-form__time">
        <FormControl error={hasErrorText(hour)} required fullWidth>
          <InputLabel id="hour-label"><Translate value="hours"/></InputLabel>
          <Select
            labelId="hour-label"
            value={hour.value}
            name="hour"
            onChange={this.setStateProperty}
            displayEmpty>
            {hoursItems}
          </Select>
          {hasErrorText(hour) ? <FormHelperText>{hour.errorText}</FormHelperText> : null}
        </FormControl>
        <FormControl error={hasErrorText(minute)} required fullWidth>
          <InputLabel id="minute-label"><Translate value="minutes"/></InputLabel>
          <Select
            labelId="minute-label"
            value={minute.value}
            name="minute"
            onChange={this.setStateProperty}
            displayEmpty>
            {minutesItems}
          </Select>
          {hasErrorText(minute) ? <FormHelperText>{minute.errorText}</FormHelperText> : null}
        </FormControl>
      </div>
      <Button
        onClick={this.createTimetable}
        disabled={this.props.pending}
        color="primary"
        variant="contained"
        fullWidth>
        {this.props.pending ? <Spinner size={24}/> : <Translate value="createTimetable"/>}
      </Button>
    </form>;

    return (
      <div className="timetable-form">
        <Paper>
          <ThemeProvider theme={theme}>{form}</ThemeProvider>
        </Paper>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  error: state.timetableReducer.error,
  pending: state.timetableReducer.pending
});

const mapDispatchToProps = {
  getTimetable
};

export default connect(mapStateToProps, mapDispatchToProps)(TimetableForm);
