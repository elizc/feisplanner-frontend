import {post, to} from "../../utils/apiServiceBase";
import history from "../../utils/history";
import {getFeisIdFromOrgUrl} from "../../utils/stringFunctions";

export const types = {
  GET_TIMETABLE_PENDING: 'GET_TIMETABLE_PENDING',
  GET_TIMETABLE_SUCCESS: 'GET_TIMETABLE_SUCCESS',
  GET_TIMETABLE_ERROR: 'GET_TIMETABLE_ERROR'
}

export const getTimetable = body => {
  return async dispatch => {
    dispatch({
      type: types.GET_TIMETABLE_PENDING
    });

    const {pathname} = history.location;
    const feisId = getFeisIdFromOrgUrl(pathname);
    const url = `/api/timetable?feis=${feisId}`;
    const [error, result] = await to(post(url, body));

    if (error) {
      return dispatch({
        type: types.GET_TIMETABLE_ERROR
      })
    }

    window.open(result.data, "_blank");
    return dispatch({
      type: types.GET_TIMETABLE_SUCCESS,
      link: result.data
    })
  }
}
