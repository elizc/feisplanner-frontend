import React, {Component} from "react";
import {Hidden} from "@material-ui/core";
import UserHeader from "../user-header/UserHeader";
import SidePanel from "../../molecules/side-panel/SidePanel";
import './MainNav.css';

class MainNav extends Component {
  constructor(props) {
    super(props);
    this.state = {
      panelOpen: false
    };
    this.toggleMobilePanel = this.toggleMobilePanel.bind(this);
  }

  toggleMobilePanel() {
    this.setState(prevState => ({
      panelOpen: !prevState.panelOpen
    }))
  }

  render() {
    return (
      <div className="main-nav">
        <UserHeader onToggleMobilePanel={this.toggleMobilePanel}/>
        <Hidden mdUp>
          <SidePanel
            variant="temporary"
            open={this.state.panelOpen}
            onMobileClose={this.toggleMobilePanel}/>
        </Hidden>
        <Hidden smDown>
          <SidePanel
            variant="permanent"
            open/>
        </Hidden>
      </div>
    );
  }
}

export default MainNav;
