import React, {Component} from "react";
import {connect} from "react-redux";
import {getAgeGroups} from "./actions";
import {Card, CardHeader, Divider, Grid, List, ListItem, ListItemText, Typography} from "@material-ui/core";
import {Translate} from "react-redux-i18n";
import SearchForm from "../../molecules/search-from/SearchForm";
import Spinner from "../../atoms/spinner/Spinner";
import {getAge} from "../../utils/dateFunctions";
import './AgeGroupsList.css';

class AgeGroupsList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filters: {}
    }
    this.setFilters = this.setFilters.bind(this);
  }

  componentDidMount() {
    const {getAgeGroups} = this.props;
    getAgeGroups();
  }

  setFilters({danceName, level}) {
    this.setState({
      filters: {
        name: danceName.toLowerCase(),
        level: level.toLowerCase()
      }
    })
  }

  render() {
    const filtersAreSet = () => {
      for (let filter in this.state.filters) {
        if (this.state.filters[filter]) return true;
      }
      return false;
    };
    const filterAgeGroups = ageGroup => {

      const {filters} = this.state;
      for (let property in filters) {
        if (!filters[property]) {
          continue;
        }
        const {dance} = ageGroup;

        if (!dance[property].toLowerCase().startsWith(filters[property])) {
          return false;
        }
      }
      return true;
    };

    let {ageGroups, pending} = this.props;
    if (filtersAreSet()) {
      ageGroups = ageGroups.filter(filterAgeGroups);
    }

    const createAgeGroupItem = ageGroup => {
      const {dance, ageDesignation, entries} = ageGroup;
      const createDancerItem = entry => {
        const {id, student, feisNumber} = entry;
        const {firstName, surname, birthDate} = student;
        return (
          <ListItem key={id}>
            <ListItemText primary={`${feisNumber} - ${surname}, ${firstName} / ${getAge(birthDate)}`}/>
          </ListItem>
        );
      };
      const dancersItems = entries.map(createDancerItem);

      const ageGroupName = `${dance.level} ${dance.name} ${ageDesignation}`;
      return <Grid item xs={12} sm={6} key={ageGroupName} zeroMinWidth>
        <Card className="age-groups-list__item">
          <CardHeader
            title={ageGroupName}
            subheader={<Translate value="countOfDancers" count={entries.length}/>}/>
          <Divider/>
          <List dense>
            {dancersItems}
          </List>
        </Card>
      </Grid>
    };
    const ageGroupItems = ageGroups.map(createAgeGroupItem);

    const searchFieldsData = [
      {label: 'nameOfDance', name: 'danceName'},
      {label: 'level', name: 'level'},
    ];

    return (
      <div className="age-groups-list">
        <SearchForm fieldsData={searchFieldsData} search={this.setFilters}/>
        <Typography paragraph>
          <Translate value="found"/> {ageGroups.length} <Translate value="of"/> {this.props.ageGroups.length}
        </Typography>
        {pending ?
          <Spinner size={40}/> :
          <Grid container spacing={2} alignItems="stretch">
            {ageGroupItems}
          </Grid>}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  ageGroups: state.ageGroupsReducer.ageGroups,
  pending: state.ageGroupsReducer.pending
});

const mapDispatchToProps = {
  getAgeGroups
};

export default connect(mapStateToProps, mapDispatchToProps)(AgeGroupsList);
