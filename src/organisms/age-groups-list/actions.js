import {get, to} from "../../utils/apiServiceBase";
import history from "../../utils/history";
import {getFeisIdFromOrgUrl} from "../../utils/stringFunctions";

export const types = {
  GET_AGE_GROUPS_PENDING: 'GET_AGE_GROUPS_PENDING',
  GET_AGE_GROUPS_SUCCESS: 'GET_AGE_GROUPS_SUCCESS',
  GET_AGE_GROUPS_ERROR: 'GET_AGE_GROUPS_ERROR',
  GET_AGE_GROUPS_FLUSH: 'GET_AGE_GROUPS_FLUSH'
}

export const getAgeGroups = () => {
  return async dispatch => {
    dispatch({
      type: types.GET_AGE_GROUPS_PENDING
    });

    const {pathname} = history.location;
    const feisId = getFeisIdFromOrgUrl(pathname);
    const url = `/api/categories?feis=${feisId}`;
    const [error, result] = await to(get(url));

    if (error) {
      return dispatch({
        type: types.GET_AGE_GROUPS_ERROR
      })
    }

    return dispatch({
      type: types.GET_AGE_GROUPS_SUCCESS,
      ageGroups: result.data
    })
  }
}

export const reset = () => {
  return dispatch => {
    dispatch({
      type: types.GET_AGE_GROUPS_FLUSH
    })
  }
};
