import {types} from "./actions";

const initState = {
  pending: false,
  success: false,
  error: false,
  ageGroups: []
};

export default (state = initState, action) => {
  switch (action.type) {
    case types.GET_AGE_GROUPS_PENDING: {
      return {
        ...state,
        pending: true,
        success: false,
        error: false
      }
    }
    case types.GET_AGE_GROUPS_SUCCESS: {
      return {
        ...state,
        pending: false,
        success: true,
        error: false,
        ageGroups: action.ageGroups
      }
    }
    case types.GET_AGE_GROUPS_ERROR: {
      return {
        ...state,
        pending: false,
        success: false,
        error: true
      }
    }
    case types.GET_AGE_GROUPS_FLUSH: {
      return {
        ...state,
        pending: false,
        success: false,
        error: false,
        feises: []
      }
    }
    default: {
      return state;
    }
  }
}
