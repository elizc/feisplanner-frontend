import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Hidden, Typography} from "@material-ui/core";
import Header from "../header/Header";
import LogoutIcon from "../../atoms/logout-icon/LogoutIcon";
import './UserHeader.css';
import * as jwt from "jsonwebtoken";
import {constants} from "../../utils/constants";

class UserHeader extends Component {
  render() {
    const nameStyle = {
      marginRight: '10px'
    };

    const token = localStorage.getItem('token');
    const parsedToken = jwt.verify(token, constants.JWT_KEY);
    const firstName = parsedToken[constants.FIRST_NAME_KEY];
    const surname = parsedToken[constants.SURNAME_KEY];

    return (
      <Header onToggleMobilePanel={this.props.onToggleMobilePanel}>
        <Hidden smDown>
          <Typography style={nameStyle}>{firstName} {surname}</Typography>
        </Hidden>
        <LogoutIcon/>
      </Header>
    )
  }
}

UserHeader.propTypes = {
  onToggleMobilePanel: PropTypes.func,
};

export default UserHeader;
