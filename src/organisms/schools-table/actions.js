import {get, post, put, remove, to} from "../../utils/apiServiceBase";

export const types = {
  GET_SCHOOLS_PENDING: 'GET_SCHOOLS_PENDING',
  GET_SCHOOLS_SUCCESS: 'GET_SCHOOLS_SUCCESS',
  GET_SCHOOLS_ERROR: 'GET_SCHOOLS_ERROR',
  GET_SCHOOLS_FLUSH: 'GET_SCHOOLS_FLUSH',
  DELETE_SCHOOL_PENDING: 'DELETE_SCHOOLS_PENDING',
  DELETE_SCHOOL_SUCCESS: 'DELETE_SCHOOLS_SUCCESS',
  DELETE_SCHOOL_ERROR: 'DELETE_SCHOOLS_ERROR',
  EDIT_SCHOOL_PENDING: 'EDIT_SCHOOL_PENDING',
  EDIT_SCHOOL_SUCCESS: 'EDIT_SCHOOL_SUCCESS',
  EDIT_SCHOOL_ERROR: 'EDIT_SCHOOL_ERROR',
  ADD_SCHOOL_PENDING: 'ADD_SCHOOL_PENDING',
  ADD_SCHOOL_SUCCESS: 'ADD_SCHOOL_SUCCESS',
  ADD_SCHOOL_ERROR: 'ADD_SCHOOL_ERROR',
};

export const getSchools = () => {
  return async dispatch => {
    dispatch({
      type: types.GET_SCHOOLS_PENDING
    });

    const schoolsUrl = '/api/schools';
    const [error, result] = await to(get(schoolsUrl));

    if (error) {
      return dispatch({
        type: types.GET_SCHOOLS_ERROR
      })
    }

    return dispatch({
      type: types.GET_SCHOOLS_SUCCESS,
      schools: result.data
    })
  }
};

export const deleteSchool = id => {
  return async dispatch => {
    dispatch({
      type: types.DELETE_SCHOOL_PENDING
    });

    const schoolUrl = `/api/schools/${id}`;
    const [error] = await to(remove(schoolUrl));

    if (error) {
      return dispatch({
        type: types.DELETE_SCHOOL_ERROR
      })
    }

    return dispatch({
      type: types.DELETE_SCHOOL_SUCCESS,
      deletedSchoolId: id
    })
  }
};

export const editSchool = editedSchool => {
  return async dispatch => {
    dispatch({
      type: types.EDIT_SCHOOL_PENDING
    });

    const [error, result] = await to(put('api/schools', editedSchool));

    if (error) {
      return dispatch({
        type: types.EDIT_SCHOOL_ERROR
      });
    }

    if (result === undefined) {
      return dispatch({
        type: types.EDIT_SCHOOL_ERROR
      });
    }

    return dispatch({
      type: types.EDIT_SCHOOL_SUCCESS,
      editedSchool: result.data
    });
  }
};

export const addSchool = school => {
  return async dispatch => {
    dispatch({
      type: types.ADD_SCHOOL_PENDING
    });

    const [error, result] = await to(post('api/schools', school));

    if (error) {
      return dispatch({
        type: types.ADD_SCHOOL_ERROR
      });
    }

    if (result === undefined) {
      return dispatch({
        type: types.ADD_SCHOOL_ERROR
      });
    }

    dispatch({
      type: types.ADD_SCHOOL_SUCCESS,
      addedSchool: result.data
    });
  }
};

export const reset = () => {
  return dispatch => {
    dispatch({
      type: types.GET_SCHOOLS_FLUSH
    })
  }
};
