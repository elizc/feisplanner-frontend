import {types} from "./actions";

const initState = {
  pending: false,
  success: false,
  error: false,
  schools: []
};

export default (state = initState, action) => {
  switch (action.type) {
    case types.GET_SCHOOLS_PENDING: {
      return {
        ...state,
        pending: true,
        success: false,
        error: false
      }
    }
    case types.GET_SCHOOLS_SUCCESS: {
      return {
        ...state,
        pending: false,
        success: true,
        error: false,
        schools: action.schools
      }
    }
    case types.GET_SCHOOLS_ERROR: {
      return {
        ...state,
        pending: false,
        success: false,
        error: true
      }
    }
    case types.GET_SCHOOLS_FLUSH: {
      return {
        ...state,
        pending: false,
        success: false,
        error: false,
        schools: []
      }
    }
    case types.DELETE_SCHOOL_PENDING: {
      return  {
        ...state,
        pending: true,
        success: false,
        error: false
      }
    }
    case types.DELETE_SCHOOL_SUCCESS: {
      return  {
        ...state,
        pending: false,
        success: true,
        error: false,
        schools: state.schools.filter(school => school.id !== action.deletedSchoolId)
      }
    }
    case types.DELETE_SCHOOL_ERROR: {
      return  {
        ...state,
        pending: false,
        success: false,
        error: true
      }
    }
    case types.EDIT_SCHOOL_PENDING: {
      return  {
        ...state,
        pending: true,
        success: false,
        error: false
      }
    }
    case types.EDIT_SCHOOL_SUCCESS: {
      let schools = [...state.schools];
      for (let i = 0; i < schools.length; i++) {
        if (schools[i].id === action.editedSchool.id) {
          schools[i] = action.editedSchool;
        }
      }
      
      return  {
        ...state,
        pending: false,
        success: true,
        error: false,
        schools: schools
      }
    }
    case types.EDIT_SCHOOL_ERROR: {
      return  {
        ...state,
        pending: false,
        success: false,
        error: true
      }
    }
    case types.ADD_SCHOOL_PENDING: {
      return  {
        ...state,
        pending: true,
        success: false,
        error: false
      }
    }
    case types.ADD_SCHOOL_SUCCESS: {
      const sortByName = (a, b) => {
        return a.name > b.name ? 1 : -1
      };

      return  {
        ...state,
        pending: false,
        success: true,
        error: false,
        schools: [...state.schools, action.addedSchool].sort(sortByName)
      }
    }
    case types.ADD_SCHOOL_ERROR: {
      return  {
        ...state,
        pending: false,
        success: false,
        error: true
      }
    }
    default: {
      return state
    }
  }
}
