import React, {Component} from 'react';
import {connect} from "react-redux";
import {
  Hidden,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TablePagination,
  TableRow,
  Typography
} from "@material-ui/core";
import {Translate} from 'react-redux-i18n';
import DeleteButton from "../../atoms/delete-button/DeleteButton";
import EditButton from "../../atoms/edit-button/EditButton";
import ConfirmDialog from "../../molecules/confirm-dialog/ConfirmDialog";
import SchoolForm from "../../molecules/school-form/SchoolForm";
import SearchForm from "../../molecules/search-from/SearchForm";
import AddFab from "../../atoms/add-fab/AddFab";
import {deleteSchool, getSchools} from "./actions";
import Spinner from "../../atoms/spinner/Spinner";
import {getPaginationText} from "../../utils/stringFunctions";
import AddButton from "../../atoms/add-button/AddButton";
import './SchoolsTable.css';

class SchoolsTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      rowsPerPage: 25,
      page: 0,
      dialogIsOpen: false,
      dialogOptions: {},
      formIsOpen: false,
      chosenSchool: null,
      filters: {}
    };
    this.changePage = this.changePage.bind(this);
    this.changeRowsPerPage = this.changeRowsPerPage.bind(this);
    this.openConfirmDialog = this.openConfirmDialog.bind(this);
    this.closeConfirmDialog = this.closeConfirmDialog.bind(this);
    this.openEditForm = this.openEditForm.bind(this);
    this.openAddForm = this.openAddForm.bind(this);
    this.closeForm = this.closeForm.bind(this);
    this.deleteSchool = this.deleteSchool.bind(this);
    this.setFilters = this.setFilters.bind(this);
  }

  componentDidMount() {
    const {getSchools} = this.props;
    getSchools();
  }

  changePage(event, newPage) {
    this.setState({
      page: newPage
    });
  }

  changeRowsPerPage(event) {
    this.setState({
      rowsPerPage: parseInt(event.target.value, 10),
      page: 0
    });
  }

  openConfirmDialog(id) {
    this.setState({
      dialogIsOpen: true,
      dialogOptions: {
        deletedSchoolId: id
      }
    });
  }

  closeConfirmDialog() {
    this.setState({
      dialogIsOpen: false,
      dialogOptions: {}
    })
  }

  openEditForm(school) {
    this.setState({
      formIsOpen: true,
      chosenSchool: school
    });
  }

  openAddForm() {
    this.setState({
      formIsOpen: true,
      chosenSchool: null
    });
  }

  closeForm() {
    this.setState({
      formIsOpen: false,
      chosenSchool: null
    });
  }

  deleteSchool({deletedSchoolId}) {
    const {deleteSchool} = this.props;
    deleteSchool(deletedSchoolId);
  }

  setFilters({name, city, region}) {
    this.setState({
      filters: {
        name: name.toLowerCase(),
        city: city.toLowerCase(),
        region: region.toLowerCase()
      }
    })
  }

  render() {
    const columnHeadings = [
      'nameOfSchool',
      'region',
      'city',
      'actions'
    ];
    const createHeadCell = heading => {
      return <TableCell
        key={heading}
        className="schools-table__head-cell"
        align="center">
        <Translate value={heading}/>
      </TableCell>
    };
    const headCells = columnHeadings.map(createHeadCell);

    const filtersAreSet = () => {
      for (let filter in this.state.filters) {
        if (this.state.filters[filter]) return true;
      }
      return false;
    };

    const filterSchools = school => {
      const {filters} = this.state;
      for (let property in filters) {
        if (!filters[property]) {
          continue;
        }

        if (!school[property].toLowerCase().startsWith(filters[property])) {
          return false;
        }
      }
      return true;
    };

    let schools;
    if (filtersAreSet()) {
      schools = this.props.schools.filter(filterSchools);
    } else {
      schools = this.props.schools;
    }

    const createSchoolRow = school => {
      return <TableRow key={school.id} hover>
        <Hidden mdUp>
          <TableCell>
            {school.name}
            <Typography color="textSecondary">{school.region}, {school.city}</Typography>
          </TableCell>
        </Hidden>
        <Hidden smDown>
          <TableCell>{school.name}</TableCell>
          <TableCell>{school.region}</TableCell>
          <TableCell>{school.city}</TableCell>
        </Hidden>
        <TableCell align="center">
          <EditButton editedEntity={school} onClick={this.openEditForm}/>
          <DeleteButton deletedId={school.id} onClick={this.openConfirmDialog}/>
        </TableCell>
      </TableRow>
    };
    const schoolsRows = schools.map(createSchoolRow);

    const searchFieldsData = [
      {label: 'nameOfSchool', name: 'name'},
      {label: 'region', name: 'region'},
      {label: 'city', name: 'city'}
    ];
    const {page, rowsPerPage, dialogIsOpen, dialogOptions, formIsOpen, chosenSchool} = this.state;

    return (
      <div className="schools-table">
        <Hidden smDown>
          <AddButton onClick={this.openAddForm}/>
        </Hidden>
        <Hidden mdUp>
          <AddFab onClick={this.openAddForm}/>
        </Hidden>
        <SearchForm fieldsData={searchFieldsData} search={this.setFilters}/>
        {this.props.pending ?
          <Spinner size={40}/> :
          <Paper>
            <TablePagination
              component="div"
              count={schoolsRows.length}
              page={page}
              rowsPerPage={rowsPerPage}
              rowsPerPageOptions={[25, 50, 100]}
              labelRowsPerPage={<Translate value="rowsPerPage"/>}
              labelDisplayedRows={getPaginationText}
              onChangePage={this.changePage}
              onChangeRowsPerPage={this.changeRowsPerPage}/>
            <TableContainer>
              <Table size="small">
                <TableHead>
                  <TableRow>
                    {headCells}
                  </TableRow>
                </TableHead>
                <TableBody>
                  {schoolsRows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)}
                </TableBody>
              </Table>
            </TableContainer>
          </Paper>}
        <ConfirmDialog
          open={dialogIsOpen}
          options={dialogOptions}
          title="confirmation.deleteSchool"
          content="warning.deleteSchool"
          confirmText="yes"
          cancelText="no"
          onClose={this.closeConfirmDialog}
          onConfirm={this.deleteSchool}/>
        <SchoolForm
          open={formIsOpen}
          onClose={this.closeForm}
          school={chosenSchool}/>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  schools: state.schoolsReducer.schools,
  pending: state.schoolsReducer.pending
});

const mapDispatchToProps = {
  getSchools,
  deleteSchool
};

export default connect(mapStateToProps, mapDispatchToProps)(SchoolsTable);
