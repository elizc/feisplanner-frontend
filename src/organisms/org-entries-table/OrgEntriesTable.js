import React, {Component} from "react";
import {connect} from "react-redux";
import {getEntries} from "./actions";
import {
  Hidden,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TablePagination,
  TableRow,
  Typography
} from "@material-ui/core";
import {getAge} from "../../utils/dateFunctions";
import {Translate} from "react-redux-i18n";
import SearchForm from "../../molecules/search-from/SearchForm";
import './OrgFeisTable.css';
import Spinner from "../../atoms/spinner/Spinner";
import {getPaginationText} from "../../utils/stringFunctions";

class OrgEntriesTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      rowsPerPage: 25,
      page: 0,
      filters: {}
    };
    this.changePage = this.changePage.bind(this);
    this.changeRowsPerPage = this.changeRowsPerPage.bind(this);
    this.setFilters = this.setFilters.bind(this);
  }

  componentDidMount() {
    const {getEntries} = this.props;
    getEntries();
  }

  changePage(event, newPage) {
    this.setState({
      page: newPage
    });
  }

  changeRowsPerPage(event) {
    this.setState({
      rowsPerPage: parseInt(event.target.value, 10),
      page: 0
    });
  }

  setFilters({schoolName, studentName, dance}) {
    this.setState({
      filters: {
        schoolName: schoolName.toLowerCase(),
        studentName: studentName.toLowerCase(),
        dance: dance.toLowerCase()
      }
    })
  }

  render() {
    const columnHeadings = [
      'dancer',
      'teacher',
      'dances'
    ];
    const createHeadCell = heading => {
      return <TableCell
        key={heading}
        className="org-entries-table__head-cell"
        align="center">
        <Translate value={heading}/>
      </TableCell>
    };
    const headCells = columnHeadings.map(createHeadCell);

    const filtersAreSet = () => {
      for (let filter in this.state.filters) {
        if (this.state.filters[filter]) return true;
      }
      return false;
    };

    const filterEntries = entry => {
      const {filters} = this.state;
      for (let property in filters) {
        if (!filters[property]) {
          continue;
        }

        switch (property) {
          case 'schoolName': {
            if (!entry.student.teacher.school.name.toLowerCase().startsWith(filters[property])) return false;
            break;
          }
          case 'studentName': {
            const {firstName, surname} = entry.student;
            if (!firstName.toLowerCase().startsWith(filters[property])
              && !surname.toLowerCase().startsWith(filters[property])) {
              return false
            }
            break;
          }
          case 'dance': {
            const hasDance = entryItem => {
              const {level, name} = entryItem.dance;
              return name.toLowerCase().startsWith(filters[property])
                || level.toLowerCase().startsWith(filters[property])
                || `${level} ${name}`.toLowerCase().startsWith(filters[property])
                || `${name} ${level}`.toLowerCase().startsWith(filters[property]);

            };
            return entry.entryItems.some(hasDance);
          }
          default: {
            return false;
          }
        }
      }
      return true;
    };

    const {page, rowsPerPage} = this.state;

    let entries;
    if (filtersAreSet()) {
      entries = this.props.entries.filter(filterEntries);
    } else {
      entries = this.props.entries;
    }
    const createEntryRow = entry => {
      const {id, student, entryItems} = entry;
      const createDance = entryItem => {
        const {id, dance} = entryItem;
        return <li key={id}>{dance.name} {dance.level}</li>
      };
      const danceItems = entryItems.map(createDance);

      return <TableRow key={id} hover>
        <Hidden mdUp>
          <TableCell>
            <Typography><b>{student.surname}, {student.firstName}</b></Typography>
            <Typography>
              <Translate value="age"/>: {getAge(student.birthDate)}
            </Typography>
            <Typography>
              <Translate value="school"/>: {student.teacher.school.name}
            </Typography>
            <Typography>
              <Translate value="teacher"/>: {student.teacher.firstName} {student.teacher.surname}
            </Typography>
          </TableCell>
        </Hidden>
        <Hidden smDown>
          <TableCell>
            <Typography><b>{student.surname}, {student.firstName}</b></Typography>
            <Typography>
              <Translate value="age"/> {getAge(student.birthDate)}
            </Typography>
          </TableCell>
          <TableCell>
            {student.teacher.firstName} {student.teacher.surname}, {student.teacher.school.name}
          </TableCell>
        </Hidden>
        <TableCell>
          <ul className="org-entries-table__dances-list">{danceItems}</ul>
        </TableCell>
      </TableRow>
    };
    const entriesRows = entries.map(createEntryRow);

    const searchFieldsData = [
      {label: 'nameOfSchool', name: 'schoolName'},
      {label: 'nameOfDancer', name: 'studentName'},
      {label: 'dance', name: 'dance'}
    ];

    return (
      <div className="org-entries-table">
        <SearchForm fieldsData={searchFieldsData} search={this.setFilters}/>
        {this.props.pending ?
          <Spinner size={40}/> :
          <Paper>
            <TablePagination
              component="div"
              count={entriesRows.length}
              page={page}
              rowsPerPage={rowsPerPage}
              rowsPerPageOptions={[25, 50, 100]}
              labelRowsPerPage={<Translate value="rowsPerPage"/>}
              labelDisplayedRows={getPaginationText}
              onChangePage={this.changePage}
              onChangeRowsPerPage={this.changeRowsPerPage}/>
            <TableContainer>
              <Table>
                <TableHead>
                  <TableRow>
                    {headCells}
                  </TableRow>
                </TableHead>
                {<TableBody>
                  {entriesRows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)}
                </TableBody>}
              </Table>
            </TableContainer>
          </Paper>}
      </div>
    )
  }
}

const mapStateToProps = state => ({
  entries: state.orgEntriesReducer.entries,
  pending: state.orgEntriesReducer.pending
});

const mapDispatchToProps = {
  getEntries
};

export default connect(mapStateToProps, mapDispatchToProps)(OrgEntriesTable);
