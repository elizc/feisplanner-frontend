import {get, to} from "../../utils/apiServiceBase";
import history from "../../utils/history";
import {getFeisIdFromOrgUrl} from "../../utils/stringFunctions";

export const types = {
  GET_ENTRIES_PENDING: 'GET_ENTRIES_PENDING',
  GET_ENTRIES_SUCCESS: 'GET_ENTRIES_SUCCESS',
  GET_ENTRIES_ERROR: 'GET_ENTRIES_ERROR',
  GET_ENTRIES_FLUSH: 'GET_ENTRIES_FLUSH'
};

export const getEntries = () => {
  return async dispatch => {
    dispatch({
      type: types.GET_ENTRIES_PENDING
    });

    const {pathname} = history.location;
    const feisId = getFeisIdFromOrgUrl(pathname);
    const url = `/api/entries/feis?id=${feisId}`;
    const [error, result] = await to(get(url));


    if (error) {
      dispatch({
        type: types.GET_ENTRIES_ERROR
      })
    }

    return dispatch({
      type: types.GET_ENTRIES_SUCCESS,
      entries: result.data
    });
  }
};

export const reset = () => {
  return dispatch => {
    dispatch({
      type: types.GET_ENTRIES_FLUSH
    })
  }
};
