import {types} from './actions';

const initState = {
  pending: false,
  success: false,
  error: false,
  entries: []
};

export default (state = initState, action) => {
  switch (action.type) {
    case types.GET_ENTRIES_PENDING: {
      return {
        ...state,
        pending: true,
        success: false,
        error: false
      }
    }
    case types.GET_ENTRIES_SUCCESS: {
      return {
        ...state,
        pending: false,
        success: true,
        error: false,
        entries: action.entries
      }
    }
    case types.GET_ENTRIES_ERROR: {
      return {
        ...state,
        pending: false,
        success: false,
        error: true,
      }
    }
    case types.GET_ENTRIES_FLUSH: {
      return {
        ...state,
        initState
      }
    }
    default: {
      return state
    }
  }
}
