import React from 'react';
import PropTypes from 'prop-types';
import Paper from "@material-ui/core/Paper";

const TabPanel = ({children, value, index}) => {
  return (
    <Paper hidden={value !== index} square>
      {children}
    </Paper>
  );
};

TabPanel.propTypes = {
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

export default TabPanel;
