import {get, post, put, remove, to} from "../../utils/apiServiceBase";

export const types = {
  GET_STUDENTS_PENDING: 'GET_STUDENTS_PENDING',
  GET_STUDENTS_SUCCESS: 'GET_STUDENTS_SUCCESS',
  GET_STUDENTS_ERROR: 'GET_STUDENTS_ERROR',
  GET_STUDENTS_FLUSH: 'GET_STUDENTS_FLUSH',
  DELETE_STUDENT_PENDING: 'DELETE_STUDENTS_PENDING',
  DELETE_STUDENT_SUCCESS: 'DELETE_STUDENTS_SUCCESS',
  DELETE_STUDENT_ERROR: 'DELETE_STUDENTS_ERROR',
  EDIT_STUDENT_PENDING: 'EDIT_STUDENT_PENDING',
  EDIT_STUDENT_SUCCESS: 'EDIT_STUDENT_SUCCESS',
  EDIT_STUDENT_ERROR: 'EDIT_STUDENT_ERROR',
  ADD_STUDENT_PENDING: 'ADD_STUDENT_PENDING',
  ADD_STUDENT_SUCCESS: 'ADD_STUDENT_SUCCESS',
  ADD_STUDENT_ERROR: 'ADD_STUDENT_ERROR',
};

export const getStudents = userId => {
  return async dispatch => {
    dispatch({
      type: types.GET_STUDENTS_PENDING
    });

    const studentsUrl = `/api/students?user=${userId}`;
    const [error, result] = await to(get(studentsUrl));

    if (error) {
      return dispatch({
        type: types.GET_STUDENTS_ERROR
      })
    }

    return dispatch({
      type: types.GET_STUDENTS_SUCCESS,
      students: result.data
    })
  }
};

export const editStudent = editedStudent => {
  return async dispatch => {
    dispatch({
      type: types.EDIT_STUDENT_PENDING
    });

    const [error, result] = await to(put('api/students', editedStudent));

    if (error) {
      return dispatch({
        type: types.EDIT_STUDENT_ERROR
      });
    }

    if (result === undefined) {
      return dispatch({
        type: types.EDIT_STUDENT_ERROR
      });
    }

    return dispatch({
      type: types.EDIT_STUDENT_SUCCESS,
      editedStudent: result.data
    });
  }
};

export const addStudent = student => {
  return async dispatch => {
    dispatch({
      type: types.ADD_STUDENT_PENDING
    });

    const [error, result] = await to(post('api/students', student));

    if (error) {
      return dispatch({
        type: types.ADD_STUDENT_ERROR
      });
    }

    dispatch({
      type: types.ADD_STUDENT_SUCCESS,
      addedStudent: result.data
    });
  }
};

export const deleteStudent = id => {
  return async dispatch => {
    dispatch({
      type: types.DELETE_STUDENT_PENDING
    });

    const studentUrl = `/api/students/${id}`;
    const [error] = await to(remove(studentUrl));

    if (error) {
      return dispatch({
        type: types.DELETE_STUDENT_ERROR
      })
    }

    return dispatch({
      type: types.DELETE_STUDENT_SUCCESS,
      deletedStudentId: id
    })
  }
};
