import React, {Component} from 'react';
import {connect} from "react-redux";
import {Localize, Translate} from "react-redux-i18n";
import {
  Hidden,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TablePagination,
  TableRow,
  Typography
} from "@material-ui/core";
import * as jwt from "jsonwebtoken";
import AddFab from "../../atoms/add-fab/AddFab";
import SearchForm from "../../molecules/search-from/SearchForm";
import StudentForm from "../../molecules/student-form/StudentForm";
import EditButton from "../../atoms/edit-button/EditButton";
import DeleteButton from "../../atoms/delete-button/DeleteButton";
import {getAge} from "../../utils/dateFunctions";
import {constants} from "../../utils/constants";
import {deleteStudent, getStudents} from "./actions";
import ConfirmDialog from "../../molecules/confirm-dialog/ConfirmDialog";
import Spinner from "../../atoms/spinner/Spinner";
import {getPaginationText} from "../../utils/stringFunctions";
import AddButton from "../../atoms/add-button/AddButton";
import './StudentsTable.css';

class StudentsTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      rowsPerPage: 25,
      page: 0,
      dialogIsOpen: false,
      dialogOptions: {},
      formIsOpen: false,
      chosenStudent: null,
      filters: {}
    };
    this.changePage = this.changePage.bind(this);
    this.changeRowsPerPage = this.changeRowsPerPage.bind(this);
    this.openConfirmDialog = this.openConfirmDialog.bind(this);
    this.closeConfirmDialog = this.closeConfirmDialog.bind(this);
    this.deleteStudent = this.deleteStudent.bind(this);
    this.closeForm = this.closeForm.bind(this);
    this.openAddForm = this.openAddForm.bind(this);
    this.openEditForm = this.openEditForm.bind(this);
    this.setFilters = this.setFilters.bind(this);
  }

  componentDidMount() {
    const token = localStorage.getItem('token');
    const parsedToken = jwt.verify(token, constants.JWT_KEY);
    const userId = parsedToken[constants.USER_ID_KEY];
    const {getStudents} = this.props;
    getStudents(userId);
  }

  changePage(event, newPage) {
    this.setState({
      page: newPage
    });
  }

  changeRowsPerPage(event) {
    this.setState({
      rowsPerPage: parseInt(event.target.value, 10),
      page: 0
    });
  }

  openConfirmDialog(id) {
    this.setState({
      dialogIsOpen: true,
      dialogOptions: {
        deletedStudentId: id
      }
    });
  }

  closeConfirmDialog() {
    this.setState({
      dialogIsOpen: false,
      dialogOptions: {}
    })
  }

  deleteStudent({deletedStudentId}) {
    const {deleteStudent} = this.props;
    deleteStudent(deletedStudentId);
  }

  openAddForm() {
    this.setState({
      formIsOpen: true,
      chosenStudent: null
    });
  }

  openEditForm(student) {
    this.setState({
      formIsOpen: true,
      chosenStudent: student
    });
  }

  closeForm() {
    this.setState({
      formIsOpen: false,
      chosenStudent: null
    });
  }

  setFilters({name, minimumAge, maximumAge}) {
    this.setState({
      filters: {
        name: name.toLowerCase(),
        minAge: minimumAge,
        maxAge: maximumAge
      }
    })
  }

  render() {
    const filtersAreSet = () => {
      for (let filter in this.state.filters) {
        if (this.state.filters[filter]) return true;
      }
      return false;
    };

    const filterStudents = student => {
      const {filters} = this.state;
      for (let property in filters) {
        if (!filters[property]) {
          continue;
        }

        switch (property) {
          case 'name': {
            const {firstName, surname} = student;
            if (!firstName.toLowerCase().startsWith(filters.name)
              && !surname.toLowerCase().startsWith(filters.name)) {
              return false
            }
            break;
          }
          case 'minAge': {
            if (!parseInt(filters.minAge)) continue;

            const age = getAge(student.birthDate);
            if (age < +filters.minAge) return false;
            break;
          }
          case 'maxAge': {
            const age = getAge(student.birthDate);
            if (+filters.maxAge < age) return false;
            break;
          }
        }
      }
      return true;
    };

    const columnHeadings = [
      'nameOfStudent',
      'age',
      'dateOfBirth',
      'actions'
    ];
    const createHeadCell = heading => {
      return <TableCell
        key={heading}
        className="students-table__head-cell"
        align="center">
        <Translate value={heading}/>
      </TableCell>
    };
    const headCells = columnHeadings.map(createHeadCell);

    let students;
    if (filtersAreSet()) {
      students = this.props.students.filter(filterStudents);
    } else {
      students = this.props.students;
    }

    const createStudentRow = student => {
      const date = <Localize value={student.birthDate} dateFormat="dateFormat"/>;
      const age = getAge(student.birthDate);
      return <TableRow key={student.id} hover>
        <Hidden mdUp>
          <TableCell>
            {student.surname}, {student.firstName}
            <Typography color="textSecondary"><Translate value="age"/> {age}</Typography>
            <Typography color="textSecondary"><Translate value="dateOfBirth"/> {date}</Typography>
          </TableCell>
        </Hidden>
        <Hidden smDown>
          <TableCell>{student.surname}, {student.firstName}</TableCell>
          <TableCell align="center">{age}</TableCell>
          <TableCell align="center">{date}</TableCell>
        </Hidden>
        <TableCell align="center">
          <EditButton editedEntity={student} onClick={this.openEditForm}/>
          <DeleteButton deletedId={student.id} onClick={this.openConfirmDialog}/>
        </TableCell>
      </TableRow>
    };
    const studentsRows = students.map(createStudentRow);

    const searchFieldsData = [
      {label: 'nameOfStudent', name: 'name'},
      {label: 'minAge', name: 'minimumAge'},
      {label: 'maxAge', name: 'maximumAge'}
    ];

    const {formIsOpen, page, rowsPerPage, dialogIsOpen, dialogOptions, chosenStudent} = this.state;

    return (
      <div className="students-table">
        <Hidden smDown>
          <AddButton onClick={this.openAddForm}/>
        </Hidden>
        <Hidden mdUp>
          <AddFab onClick={this.openAddForm}/>
        </Hidden>
        <SearchForm fieldsData={searchFieldsData} search={this.setFilters}/>
        {this.props.pending ?
          <Spinner size={40}/> :
          <Paper>
            <TablePagination
              component="div"
              count={studentsRows.length}
              page={page}
              rowsPerPage={rowsPerPage}
              rowsPerPageOptions={[25, 50, 100]}
              labelRowsPerPage={<Translate value="rowsPerPage"/>}
              labelDisplayedRows={getPaginationText}
              onChangePage={this.changePage}
              onChangeRowsPerPage={this.changeRowsPerPage}/>
            <TableContainer>
              <Table size="small">
                <TableHead>
                  <TableRow>
                    {headCells}
                  </TableRow>
                </TableHead>
                <TableBody>
                  {studentsRows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)}
                </TableBody>
              </Table>
            </TableContainer>
          </Paper>}
        <ConfirmDialog
          open={dialogIsOpen}
          options={dialogOptions}
          title="confirmation.deleteStudent"
          confirmText="yes"
          cancelText="no"
          onClose={this.closeConfirmDialog}
          onConfirm={this.deleteStudent}/>
        <StudentForm
          open={formIsOpen}
          onClose={this.closeForm}
          student={chosenStudent}/>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  students: state.studentsReducer.students,
  pending: state.studentsReducer.pending
});

const mapDispatchToProps = {
  getStudents,
  deleteStudent
};

export default connect(mapStateToProps, mapDispatchToProps)(StudentsTable);
