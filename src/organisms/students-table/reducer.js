import {types} from "./actions";

const initState = {
  pending: false,
  success: false,
  error: false,
  students: []
};

export default (state = initState, action) => {
  switch (action.type) {
    case types.GET_STUDENTS_PENDING: {
      return {
        ...state,
        pending: true,
        success: false,
        error: false
      }
    }
    case types.GET_STUDENTS_SUCCESS: {
      return {
        ...state,
        pending: false,
        success: true,
        error: false,
        students: action.students
      }
    }
    case types.GET_STUDENTS_ERROR: {
      return {
        ...state,
        pending: false,
        success: false,
        error: true
      }
    }
    case types.GET_STUDENTS_FLUSH: {
      return {
        ...state,
        pending: false,
        success: false,
        error: false,
        students: []
      }
    }
    case types.EDIT_STUDENT_PENDING: {
      return  {
        ...state,
        pending: true,
        success: false,
        error: false
      }
    }
    case types.EDIT_STUDENT_SUCCESS: {
      let students = [...state.students];
      for (let i = 0; i < students.length; i++) {
        if (students[i].id === action.editedStudent.id) {
          students[i] = action.editedStudent;
        }
      }

      return  {
        ...state,
        pending: false,
        success: true,
        error: false,
        students: students
      }
    }
    case types.EDIT_STUDENT_ERROR: {
      return  {
        ...state,
        pending: false,
        success: false,
        error: true
      }
    }
    case types.ADD_STUDENT_PENDING: {
      return {
        ...state,
        pending: true,
        success: false,
        error: false
      }
    }
    case types.ADD_STUDENT_SUCCESS: {
      const sortByFirstName = (a, b) => {
        return a.surname > b.surname ? 1 : -1
      };

      return {
        ...state,
        pending: false,
        success: true,
        error: false,
        students: [...state.students, action.addedStudent].sort(sortByFirstName)
      }
    }
    case types.ADD_STUDENT_ERROR: {
      return {
        ...state,
        pending: false,
        success: false,
        error: true
      }
    }
    case types.DELETE_STUDENT_PENDING: {
      return  {
        ...state,
        pending: true,
        success: false,
        error: false
      }
    }
    case types.DELETE_STUDENT_SUCCESS: {
      return  {
        ...state,
        pending: false,
        success: true,
        error: false,
        students: state.students.filter(student => student.id !== action.deletedStudentId)
      }
    }
    case types.DELETE_STUDENT_ERROR: {
      return  {
        ...state,
        pending: false,
        success: false,
        error: true
      }
    }
    default: {
      return state
    }
  }
}
