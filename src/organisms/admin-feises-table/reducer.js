import {types} from "./actions";

const initState = {
  pending: false,
  success: false,
  error: false,
  feises: []
};

export default (state = initState, action) => {
  switch (action.type) {
    case types.GET_FEISES_PENDING: {
      return {
        ...state,
        pending: true,
        success: false,
        error: false
      }
    }
    case types.GET_FEISES_SUCCESS: {
      return {
        ...state,
        pending: false,
        success: true,
        error: false,
        feises: action.feises
      }
    }
    case types.GET_FEISES_ERROR: {
      return {
        ...state,
        pending: false,
        success: false,
        error: true
      }
    }
    case types.GET_FEISES_FLUSH: {
      return {
        ...state,
        pending: false,
        success: false,
        error: false,
        feises: []
      }
    }
    case types.ADD_FEIS_PENDING: {
      return  {
        ...state,
        pending: true,
        success: false,
        error: false
      }
    }
    case types.ADD_FEIS_SUCCESS: {
      const sortByStartDate = (a, b) => {
        const startDateA = Date.parse(a.startDate);
        const startDateB = Date.parse(b.startDate);
        return startDateA > startDateB ? 1 : -1
      };

      return  {
        ...state,
        pending: false,
        success: true,
        error: false,
        feises: [...state.feises, action.addedFeis].sort(sortByStartDate)
      }
    }
    case types.ADD_FEIS_ERROR: {
      return  {
        ...state,
        pending: false,
        success: false,
        error: true
      }
    }
    case types.EDIT_FEIS_PENDING: {
      return  {
        ...state,
        pending: true,
        success: false,
        error: false
      }
    }
    case types.EDIT_FEIS_SUCCESS: {
      let feises = [...state.feises];
      for (let i = 0; i < feises.length; i++) {
        if (feises[i].id === action.editedFeis.id) {
          feises[i] = action.editedFeis;
        }
      }

      return  {
        ...state,
        pending: false,
        success: true,
        error: false,
        feises: feises
      }
    }
    case types.EDIT_FEIS_ERROR: {
      return  {
        ...state,
        pending: false,
        success: false,
        error: true
      }
    }
    case types.DELETE_FEIS_PENDING: {
      return  {
        ...state,
        pending: true,
        success: false,
        error: false
      }
    }
    case types.DELETE_FEIS_SUCCESS: {
      return  {
        ...state,
        pending: false,
        success: true,
        error: false,
        feises: state.feises.filter(feis => feis.id !== action.deletedFeisId)
      }
    }
    case types.DELETE_FEIS_ERROR: {
      return  {
        ...state,
        pending: false,
        success: false,
        error: true
      }
    }
    default: {
      return state;
    }
  }
}
