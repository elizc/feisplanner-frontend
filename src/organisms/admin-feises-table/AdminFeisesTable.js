import React, {Component} from "react";
import {connect} from "react-redux";
import {deleteFeis, getFeises} from "./actions";
import {Localize, Translate} from "react-redux-i18n";
import {
  Hidden,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TablePagination,
  TableRow,
  Typography
} from "@material-ui/core";
import EditButton from "../../atoms/edit-button/EditButton";
import DeleteButton from "../../atoms/delete-button/DeleteButton";
import AddFab from "../../atoms/add-fab/AddFab";
import FeisForm from "../../molecules/feis-form/FeisForm";
import ConfirmDialog from "../../molecules/confirm-dialog/ConfirmDialog";
import SearchForm from "../../molecules/search-from/SearchForm";
import Spinner from "../../atoms/spinner/Spinner";
import {getPaginationText} from "../../utils/stringFunctions";
import AddButton from "../../atoms/add-button/AddButton";
import './AdminFeisesTable.css';

export class AdminFeisesTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      rowsPerPage: 25,
      page: 0,
      dialogIsOpen: false,
      dialogOptions: {},
      formIsOpen: false,
      chosenFeis: null,
      filters: {}
    };
    this.changePage = this.changePage.bind(this);
    this.changeRowsPerPage = this.changeRowsPerPage.bind(this);
    this.openConfirmDialog = this.openConfirmDialog.bind(this);
    this.closeConfirmDialog = this.closeConfirmDialog.bind(this);
    this.openAddForm = this.openAddForm.bind(this);
    this.openEditForm = this.openEditForm.bind(this);
    this.closeForm = this.closeForm.bind(this);
    this.deleteFeis = this.deleteFeis.bind(this);
    this.setFilters = this.setFilters.bind(this);
  }

  componentDidMount() {
    const {getFeises} = this.props;
    getFeises();
  }

  changePage(event, newPage) {
    this.setState({
      page: newPage
    });
  }

  changeRowsPerPage(event) {
    this.setState({
      rowsPerPage: parseInt(event.target.value, 10),
      page: 0
    });
  }

  openConfirmDialog(id) {
    this.setState({
      dialogIsOpen: true,
      dialogOptions: {
        deletedFeisId: id
      }
    });
  }

  closeConfirmDialog() {
    this.setState({
      dialogIsOpen: false,
      dialogOptions: {}
    })
  }

  openAddForm() {
    this.setState({
      formIsOpen: true,
      chosenFeis: null
    });
  }

  openEditForm(feis) {
    this.setState({
      formIsOpen: true,
      chosenFeis: feis
    });
  }

  closeForm() {
    this.setState({
      formIsOpen: false,
      chosenSFeis: null
    });
  }

  deleteFeis({deletedFeisId}) {
    const {deleteFeis} = this.props;
    deleteFeis(deletedFeisId);
  }

  setFilters({feisName, schoolName, region}) {
    this.setState({
      filters: {
        feisName: feisName.toLowerCase(),
        schoolName: schoolName.toLowerCase(),
        region: region.toLowerCase()
      }
    })
  }

  render() {
    const columnHeadings = [
      'nameOfFeis',
      'date',
      'location',
      'school',
      'actions'
    ];
    const createHeadCell = heading => {
      return <TableCell
        key={heading}
        className="admin-feises-table__head-cell"
        align="center">
        <Translate value={heading}/>
      </TableCell>
    };
    const headCells = columnHeadings.map(createHeadCell);

    const filtersAreSet = () => {
      for (let filter in this.state.filters) {
        if (this.state.filters[filter]) return true;
      }
      return false;
    };

    const filterFeises = feis => {
      const {filters} = this.state;
      for (let property in filters) {
        if (!filters[property]) {
          continue;
        }

        switch (property) {
          case 'feisName': {
            if (!feis.name.toLowerCase().startsWith(filters[property])) return false;
            break;
          }
          case 'region': {
            if (!feis.school.region.toLowerCase().startsWith(filters[property])) return false;
            break;
          }
          case 'schoolName': {
            if (!feis.school.name.toLowerCase().startsWith(filters[property])) return false;
            break;
          }
        }
      }
      return true;
    };

    let feises;
    if (filtersAreSet()) {
      feises = this.props.feises.filter(filterFeises);
    } else {
      feises = this.props.feises;
    }

    const createFeisRow = feis => {
      const startDate = <Localize value={feis.startDate} dateFormat="dateFormat"/>;

      return <TableRow key={feis.id} hover>
        <Hidden mdUp>
          <TableCell>
            {feis.name}
            <Typography color="textSecondary">{startDate}</Typography>
            <Typography color="textSecondary">{feis.school.city}, {feis.school.region}</Typography>
            <Typography color="textSecondary">{feis.school.name}</Typography>
          </TableCell>
        </Hidden>
        <Hidden smDown>
          <TableCell>{feis.name}</TableCell>
          <TableCell align="center">{startDate}</TableCell>
          <TableCell>{feis.school.city}, {feis.school.region}</TableCell>
          <TableCell>{feis.school.name}</TableCell>
        </Hidden>
        <TableCell align="center">
          <EditButton editedEntity={feis} onClick={this.openEditForm}/>
          <DeleteButton deletedId={feis.id} onClick={this.openConfirmDialog}/>
        </TableCell>
      </TableRow>
    };
    const feisesRows = feises.map(createFeisRow);

    const searchFieldsData = [
      {label: 'nameOfFeis', name: 'feisName'},
      {label: 'region', name: 'region'},
      {label: 'school', name: 'schoolName'}
    ];

    const {page, rowsPerPage, formIsOpen, chosenFeis, dialogIsOpen, dialogOptions} = this.state;

    return (
      <div className="admin-feises-table">
        <Hidden smDown>
          <AddButton onClick={this.openAddForm}/>
        </Hidden>
        <Hidden mdUp>
          <AddFab onClick={this.openAddForm}/>
        </Hidden>
        <SearchForm fieldsData={searchFieldsData} search={this.setFilters}/>
        {this.props.pending ?
          <Spinner size={40}/> :
          <Paper>
            <TablePagination
              component="div"
              count={feisesRows.length}
              page={page}
              rowsPerPage={rowsPerPage}
              rowsPerPageOptions={[25, 50, 100]}
              labelRowsPerPage={<Translate value="rowsPerPage"/>}
              labelDisplayedRows={getPaginationText}
              onChangePage={this.changePage}
              onChangeRowsPerPage={this.changeRowsPerPage}/>
            <TableContainer>
              <Table size="small">
                <TableHead>
                  <TableRow>
                    {headCells}
                  </TableRow>
                </TableHead>
                <TableBody>
                  {feisesRows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)}
                </TableBody>
              </Table>
            </TableContainer>
          </Paper>}
        <ConfirmDialog
          open={dialogIsOpen}
          options={dialogOptions}
          title="confirmation.deleteFeis"
          content="warning.deleteFeis"
          confirmText="yes"
          cancelText="no"
          onClose={this.closeConfirmDialog}
          onConfirm={this.deleteFeis}/>
        <FeisForm
          open={formIsOpen}
          onClose={this.closeForm}
          feis={chosenFeis}/>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  feises: state.feisesReducer.feises,
  pending: state.feisesReducer.pending
});

const mapDispatchToProps = {
  getFeises,
  deleteFeis
};

export default connect(mapStateToProps, mapDispatchToProps)(AdminFeisesTable);
