import {get, post, put, remove, to} from "../../utils/apiServiceBase";

export const types = {
  GET_FEISES_PENDING: 'GET_FEISES_PENDING',
  GET_FEISES_SUCCESS: 'GET_FEISES_SUCCESS',
  GET_FEISES_ERROR: 'GET_FEISES_ERROR',
  GET_FEISES_FLUSH: 'GET_FEISES_FLUSH',
  DELETE_FEIS_PENDING: 'DELETE_FEISES_PENDING',
  DELETE_FEIS_SUCCESS: 'DELETE_FEISES_SUCCESS',
  DELETE_FEIS_ERROR: 'DELETE_FEISES_ERROR',
  EDIT_FEIS_PENDING: 'EDIT_FEIS_PENDING',
  EDIT_FEIS_SUCCESS: 'EDIT_FEIS_SUCCESS',
  EDIT_FEIS_ERROR: 'EDIT_FEIS_ERROR',
  ADD_FEIS_PENDING: 'ADD_FEIS_PENDING',
  ADD_FEIS_SUCCESS: 'ADD_FEIS_SUCCESS',
  ADD_FEIS_ERROR: 'ADD_FEIS_ERROR',
};

export const getFeises = () => {
  return async dispatch => {
    dispatch({
      type: types.GET_FEISES_PENDING
    });

    const feisesUrl = '/api/feises/unfinished';
    const [error, result] = await to(get(feisesUrl));

    if (error) {
      return dispatch({
        type: types.GET_FEISES_ERROR
      })
    }

    return dispatch({
      type: types.GET_FEISES_SUCCESS,
      feises: result.data
    })
  }
};

export const addFeis = feis => {
  return async dispatch => {
    dispatch({
      type: types.ADD_FEIS_PENDING
    });

    const [error, result] = await to(post('api/feises', feis));

    if (error) {
      return dispatch({
        type: types.ADD_FEIS_ERROR
      });
    }

    if (result === undefined) {
      return dispatch({
        type: types.ADD_FEIS_ERROR
      });
    }

    dispatch({
      type: types.ADD_FEIS_SUCCESS,
      addedFeis: result.data
    });
  }
};

export const editFeis = (id, editedFeis) => {
  return async dispatch => {
    dispatch({
      type: types.EDIT_FEIS_PENDING
    });

    const [error, result] = await to(put(`api/feises/${id}`, editedFeis));

    if (error) {
      return dispatch({
        type: types.EDIT_FEIS_ERROR
      });
    }

    if (result === undefined) {
      return dispatch({
        type: types.EDIT_FEIS_ERROR
      });
    }

    return dispatch({
      type: types.EDIT_FEIS_SUCCESS,
      editedFeis: result.data
    });
  }
};

export const deleteFeis = id => {
  return async dispatch => {
    dispatch({
      type: types.DELETE_FEIS_PENDING
    });

    const url = `/api/feises/${id}`;
    const [error] = await to(remove(url));

    if (error) {
      return dispatch({
        type: types.DELETE_FEIS_ERROR
      })
    }

    return dispatch({
      type: types.DELETE_FEIS_SUCCESS,
      deletedFeisId: id
    })
  }
};

export const reset = () => {
  return dispatch => {
    dispatch({
      type: types.GET_FEISES_FLUSH
    })
  }
};
