import React, {Component} from "react";
import {connect} from "react-redux";
import {
  Hidden,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TablePagination,
  TableRow,
  Typography
} from "@material-ui/core";
import EditButton from "../../atoms/edit-button/EditButton";
import DeleteButton from "../../atoms/delete-button/DeleteButton";
import SearchForm from "../../molecules/search-from/SearchForm";
import EntryForm from "../../molecules/entry-form/EntryForm";
import {registrationIsOpen} from "../../utils/dateFunctions";
import AddFab from "../../atoms/add-fab/AddFab";
import ConfirmDialog from "../../molecules/confirm-dialog/ConfirmDialog";
import {deleteEntry, getEntries} from "./actions";
import Spinner from "../../atoms/spinner/Spinner";
import AddButton from "../../atoms/add-button/AddButton";
import './TeacherEntriesTable.css';

class TeacherEntriesTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      rowsPerPage: 30,
      page: 0,
      dialogIsOpen: false,
      dialogOptions: {},
      formIsOpen: false,
      chosenEntry: null,
      filters: {}
    };
    this.changePage = this.changePage.bind(this);
    this.changeRowsPerPage = this.changeRowsPerPage.bind(this);
    this.openConfirmDialog = this.openConfirmDialog.bind(this);
    this.closeConfirmDialog = this.closeConfirmDialog.bind(this);
    this.closeForm = this.closeForm.bind(this);
    this.openAddForm = this.openAddForm.bind(this);
    this.openEditForm = this.openEditForm.bind(this);
    this.deleteEntry = this.deleteEntry.bind(this);
    this.setFilters = this.setFilters.bind(this);
  }

  componentDidMount() {
    const {getEntries} = this.props;
    getEntries();
  }

  changePage(event, newPage) {
    this.setState({
      page: newPage
    });
  }

  changeRowsPerPage(event) {
    this.setState({
      rowsPerPage: parseInt(event.target.value, 10),
      page: 0
    });
  }

  openConfirmDialog(id) {
    this.setState({
      dialogIsOpen: true,
      dialogOptions: {
        deletedEntryId: id
      }
    });
  }

  closeConfirmDialog() {
    this.setState({
      dialogIsOpen: false,
      dialogOptions: {}
    })
  }

  openAddForm() {
    this.setState({
      formIsOpen: true,
      chosenEntry: null
    });
  }

  openEditForm(entry) {
    this.setState({
      formIsOpen: true,
      chosenEntry: entry
    });
  }

  closeForm() {
    this.setState({
      formIsOpen: false,
      chosenEntry: null
    });
  }

  deleteEntry({deletedEntryId}) {
    const {deleteEntry} = this.props;
    deleteEntry(deletedEntryId);
  }

  setFilters({feisName, studentName}) {
    this.setState({
      filters: {
        feisName: feisName.toLowerCase(),
        studentName: studentName.toLowerCase()
      }
    })
  }

  render() {
    const filtersAreSet = () => {
      for (let filter in this.state.filters) {
        if (this.state.filters[filter]) return true;
      }
      return false;
    };

    const filterEntries = entry => {
      const {filters} = this.state;
      for (let property in filters) {
        if (!filters[property]) {
          continue;
        }

        switch (property) {
          case 'feisName': {
            if (!entry.feis.name.toLowerCase().startsWith(filters[property])) return false;
            break;
          }
          case 'studentName': {
            const {firstName, surname} = entry.student;
            if (!firstName.toLowerCase().startsWith(filters[property])
              && !surname.toLowerCase().startsWith(filters[property])) {
              return false
            }
            break;
          }
          default: {
            return false;
          }
        }
      }
      return true;
    };

    let entries;
    if (filtersAreSet()) {
      entries = this.props.entries.filter(filterEntries);
    } else {
      entries = this.props.entries;
    }
    const createEntryRow = entry => {
      const {id, feis, student, entryItems} = entry;
      const createDance = entryItem => {
        const {id, dance} = entryItem;
        return <li key={id}>{dance.name} {dance.level}</li>
      };
      const danceItems = entryItems.map(createDance);

      return <TableRow key={id} hover>
        <Hidden mdUp>
          <TableCell>
            <Typography><b>{student.surname}, {student.firstName}</b></Typography>
            <Typography paragraph>{feis.name}</Typography>
            <ul className="teacher-entries-table__dances-list">{danceItems}</ul>
          </TableCell>
        </Hidden>
        <Hidden smDown>
          <TableCell>
            <Typography><b>{student.surname}, {student.firstName}</b></Typography>
            <Typography>{feis.name}</Typography>
          </TableCell>
          <TableCell>
            <ul className="teacher-entries-table__dances-list">{danceItems}</ul>
          </TableCell>
        </Hidden>
        {registrationIsOpen(feis) ?
          <TableCell align="center">
            <EditButton editedEntity={entry} onClick={this.openEditForm}/>
            <DeleteButton deletedId={entry.id} onClick={this.openConfirmDialog}/>
          </TableCell> :
          null}
      </TableRow>
    };
    const entriesRows = entries.map(createEntryRow);

    const searchFieldsData = [
      {label: 'nameOfFeis', name: 'feisName'},
      {label: 'nameOfStudent', name: 'studentName'}
    ];

    const {page, rowsPerPage, formIsOpen, chosenEntry, dialogOptions, dialogIsOpen} = this.state;

    return (
      <div className="teacher-feises-table">
        <Hidden smDown>
          <AddButton onClick={this.openAddForm}/>
        </Hidden>
        <Hidden mdUp>
          <AddFab onClick={this.openAddForm}/>
        </Hidden>
        <SearchForm fieldsData={searchFieldsData} search={this.setFilters}/>
        {this.props.pending ?
          <Spinner size={40}/> :
          <Paper>
          <TableContainer>
            <Table>
              <TableBody>
                {entriesRows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)}
              </TableBody>
            </Table>
          </TableContainer>
          <TablePagination
            component="div"
            count={entriesRows.length}
            page={page}
            rowsPerPage={rowsPerPage}
            rowsPerPageOptions={[30, 50, 100]}
            onChangePage={this.changePage}
            onChangeRowsPerPage={this.changeRowsPerPage}/>
        </Paper>}
        <ConfirmDialog
          open={dialogIsOpen}
          options={dialogOptions}
          title="confirmation.deleteEntry"
          confirmText="yes"
          cancelText="no"
          onClose={this.closeConfirmDialog}
          onConfirm={this.deleteEntry}/>
        <EntryForm
          open={formIsOpen}
          onClose={this.closeForm}
          entry={chosenEntry}/>
      </div>
    )
  }

}

const mapStateToProps = state => ({
  entries: state.teacherEntriesReducer.entries,
  pending: state.teacherEntriesReducer.pending
});

const mapDispatchToProps = {
  getEntries,
  deleteEntry
};

export default connect(mapStateToProps, mapDispatchToProps)(TeacherEntriesTable);
