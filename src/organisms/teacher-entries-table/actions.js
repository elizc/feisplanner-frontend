import * as jwt from "jsonwebtoken";
import {constants} from "../../utils/constants";
import {get, post, put, remove, to} from "../../utils/apiServiceBase";

export const types = {
  GET_ENTRIES_PENDING: 'GET_ENTRIES_PENDING',
  GET_ENTRIES_SUCCESS: 'GET_ENTRIES_SUCCESS',
  GET_ENTRIES_ERROR: 'GET_ENTRIES_ERROR',
  GET_ENTRIES_FLUSH: 'GET_ENTRIES_FLUSH',
  ADD_ENTRY_PENDING: 'ADD_ENTRY_PENDING',
  ADD_ENTRY_SUCCESS: 'ADD_ENTRY_SUCCESS',
  ADD_ENTRY_ERROR: 'ADD_ENTRY_ERROR',
  DELETE_ENTRY_PENDING: 'DELETE_ENTRY_PENDING',
  DELETE_ENTRY_SUCCESS: 'DELETE_ENTRY_SUCCESS',
  DELETE_ENTRY_ERROR: 'DELETE_ENTRY_ERROR',
  EDIT_ENTRY_PENDING: 'EDIT_ENTRY_PENDING',
  EDIT_ENTRY_SUCCESS: 'EDIT_ENTRY_SUCCESS',
  EDIT_ENTRY_ERROR: 'EDIT_ENTRY_ERROR'
};

export const getEntries = () => {
  return async dispatch => {
    dispatch({
      type: types.GET_ENTRIES_PENDING
    });

    const token = localStorage.getItem('token');
    const parsedToken = jwt.verify(token, constants.JWT_KEY);
    const userId = parsedToken[constants.USER_ID_KEY];
    const url = `/api/entries?user=${userId}`;
    const [error, result] = await to(get(url));


    if (error) {
      dispatch({
        type: types.GET_ENTRIES_ERROR
      })
    }

    return dispatch({
      type: types.GET_ENTRIES_SUCCESS,
      entries: result.data
    });
  }
};

export const addEntry = entry => {
  return async dispatch => {
    dispatch({
      type: types.ADD_ENTRY_PENDING
    });

    const [error, result] = await to(post('api/entries', entry));

    if (error || result === undefined) {
      return dispatch({
        type: types.ADD_ENTRY_ERROR
      });
    }

    return dispatch({
      type: types.ADD_ENTRY_SUCCESS,
      addedEntry: result.data
    })
  }
};

export const editEntry = (id, editedEntry) => {
  return async dispatch => {
    dispatch({
      type: types.EDIT_ENTRY_PENDING
    });

    const [error, result] = await to(put(`api/entries/${id}`, editedEntry));

    if (error || result === undefined) {
      return dispatch({
        type: types.EDIT_ENTRY_ERROR
      });
    }

    return dispatch({
      type: types.EDIT_ENTRY_SUCCESS,
      editedEntry: result.data
    });
  }
};

export const deleteEntry = id => {
  return async dispatch => {
    dispatch({
      type: types.DELETE_ENTRY_PENDING
    });

    const url = `/api/entries/${id}`;
    const [error] = await to(remove(url));

    if (error) {
      return dispatch({
        type: types.DELETE_ENTRY_ERROR
      })
    }

    return dispatch({
      type: types.DELETE_ENTRY_SUCCESS,
      deletedEntryId: id
    })
  }
};

export const reset = () => {
  return dispatch => {
    dispatch({
      type: types.GET_ENTRIES_FLUSH
    })
  }
};
