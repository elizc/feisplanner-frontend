import {types} from './actions';

const initState = {
  pending: false,
  success: false,
  error: false,
  entries: []
};

const sortByFeisDateAndStudentSurname = (a, b) => {
  const startDateA = Date.parse(a.feis.startDate);
  const startDateB = Date.parse(b.feis.startDate);

  if (startDateA === startDateB) {
    const surnameA = a.student.surname;
    const surnameB = b.student.surname;
    return surnameA > surnameB ? 1 : -1;
  }
  return startDateA > startDateB ? 1 : -1
};

export default (state = initState, action) => {
  switch (action.type) {
    case types.GET_ENTRIES_PENDING: {
      return {
        ...state,
        pending: true,
        success: false,
        error: false
      }
    }
    case types.GET_ENTRIES_SUCCESS: {
      return {
        ...state,
        pending: false,
        success: true,
        error: false,
        entries: action.entries.sort(sortByFeisDateAndStudentSurname)
      }
    }
    case types.GET_ENTRIES_ERROR: {
      return {
        ...state,
        pending: false,
        success: false,
        error: true,
      }
    }
    case types.GET_ENTRIES_FLUSH: {
      return {
        ...state,
        initState
      }
    }
    case types.ADD_ENTRY_PENDING: {
      return {
        ...state,
        pending: true,
        success: false,
        error: false
      }
    }
    case types.ADD_ENTRY_SUCCESS: {
      return {
        ...state,
        pending: false,
        success: true,
        error: false,
        entries: [...state.entries, action.addedEntry].sort(sortByFeisDateAndStudentSurname)
      }
    }
    case types.ADD_STUDENT_ERROR: {
      return {
        ...state,
        pending: false,
        success: false,
        error: true
      }
    }
    case types.EDIT_ENTRY_PENDING: {
      return  {
        ...state,
        pending: true,
        success: false,
        error: false
      }
    }
    case types.EDIT_ENTRY_SUCCESS: {
      let entries = [...state.entries];
      for (let i = 0; i < entries.length; i++) {
        if (entries[i].id === action.editedEntry.id) {
          entries[i] = action.editedEntry;
        }
      }

      return  {
        ...state,
        pending: false,
        success: true,
        error: false,
        entries: entries.sort(sortByFeisDateAndStudentSurname)
      }
    }
    case types.EDIT_ENTRY_ERROR: {
      return  {
        ...state,
        pending: false,
        success: false,
        error: true
      }
    }
    case types.DELETE_ENTRY_PENDING: {
      return  {
        ...state,
        pending: true,
        success: false,
        error: false
      }
    }
    case types.DELETE_ENTRY_SUCCESS: {
      return  {
        ...state,
        pending: false,
        success: true,
        error: false,
        entries: state.entries.filter(entry => entry.id !== action.deletedEntryId)
      }
    }
    case types.DELETE_ENTRY_ERROR: {
      return  {
        ...state,
        pending: false,
        success: false,
        error: true
      }
    }
    default: {
      return state
    }
  }
}
