import {get, postNoToken, to} from "../../utils/apiServiceBase";

export const types = {
  GET_SCHOOLS_NAMES_PENDING: 'GET_SCHOOLS_NAMES_PENDING',
  GET_SCHOOLS_NAMES_SUCCESS: 'GET_SCHOOLS_NAMES_SUCCESS',
  GET_SCHOOLS_NAMES_ERROR: 'GET_SCHOOLS_NAMES_ERROR',
  GET_SCHOOLS_NAMES_FLUSH: 'GET_SCHOOLS_NAMES_FLUSH',
  REGISTER_PENDING: 'REGISTER_PENDING',
  REGISTER_SUCCESS: 'REGISTER_SUCCESS',
  REGISTER_ERROR: 'REGISTER_ERROR',
  REGISTER_FLUSH: 'REGISTER_FLUSH'
};

export const getSchoolsNames = () => {
  return async dispatch => {
    dispatch({
      type: types.GET_SCHOOLS_NAMES_PENDING
    });

    const testsUrl = '/api/schools/names';
    const [error, result] = await to(get(testsUrl));

    if (error) {
      return dispatch({
        type: types.GET_SCHOOLS_NAMES_ERROR
      })
    }

    return dispatch({
      type: types.GET_SCHOOLS_NAMES_SUCCESS,
      schoolsNames: result.data
    })
  }
};

export const register = data => {
  return async dispatch => {
    dispatch({
      type: types.REGISTER_PENDING
    });

    const [error, result] = await to(postNoToken('/api/sign-up', data));

    if (error) {
      return dispatch({
        type: types.REGISTER_ERROR
      });
    }

    if (result.status === "UNPROCESSABLE_ENTITY") {
      return dispatch({
        type: types.REGISTER_ERROR
      });
    }

    return dispatch({
      type: types.REGISTER_SUCCESS,
      token: result.token
    });
  }
};

export const reset = () => {
  return dispatch => {
    dispatch({
      type: types.GET_SCHOOLS_NAMES_FLUSH
    });
    dispatch({
      type: types.REGISTER_FLUSH
    })
  }
};
