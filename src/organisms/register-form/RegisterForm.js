import React, {Component} from "react";
import {connect} from "react-redux";
import {Button, FormControl, FormHelperText, InputLabel, MenuItem, Select, TextField} from "@material-ui/core";
import ThemeProvider from "@material-ui/styles/ThemeProvider";
import {Translate} from 'react-redux-i18n';
import theme from "../../utils/theme";
import {isEqual} from "../../utils/comparator";
import {containsOnlyLatinLetters, exists, isEmail, isPassword} from "../../utils/formCheckers";
import {makeFirstLettersBig} from "../../utils/stringFunctions";
import history from "../../utils/history";
import * as paths from "../../utils/paths";
import {getSchoolsNames, register, reset} from './actions';
import './RegisterForm.css';
import Spinner from "../../atoms/spinner/Spinner";

class RegisterForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      registerError: false,
      email: {
        value: '',
        errorText: '',
        checkers: [exists, isEmail]
      },
      password: {
        value: '',
        errorText: '',
        checkers: [exists, isPassword]
      },
      firstName: {
        value: '',
        errorText: '',
        checkers: [exists, containsOnlyLatinLetters]
      },
      surname: {
        value: '',
        errorText: '',
        checkers: [exists, containsOnlyLatinLetters]
      },
      school: {
        value: '',
        errorText: '',
        checkers: [exists]
      },
    };
    this.setStateProperty = this.setStateProperty.bind(this);
    this.register = this.register.bind(this);
  }

  componentDidMount() {
    const {getSchoolsNames} = this.props;
    getSchoolsNames();
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    const {success} = this.props;
    if (success) {
      this.props.reset();
      history.push(paths.STUDENTS);
    }
    if (!isEqual(prevProps, this.props) && this.props.error) {
      this.setState({
        registerError: true
      });
    }
  }

  setStateProperty(event) {
    const {target} = event;
    const property = target.name;
    const value = target.value;

    this.setState(prevState => ({
      registerError: false,
      [property]: {
        ...prevState[property],
        value
      }
    }));
  }

  register(event) {
    const getErrorsCount = () => {
      let errorsCount = 0;
      for (let property in this.state) {
        if (!this.state[property].hasOwnProperty('checkers')) continue;

        const {value} = this.state[property];
        const errorText = this.check(property, value);
        if (errorText) {
          errorsCount++;
          this.setState({
            [property]: {
              ...this.state[property],
              errorText
            }
          })
        } else {
          this.setState({
            [property]: {
              ...this.state[property],
              errorText: ''
            }
          })
        }
      }
      return errorsCount;
    };

    event.preventDefault();
    if (getErrorsCount() !== 0) {
      return false;
    }

    const {email, password, surname, firstName, school} = this.state;
    const body = {
      email: email.value,
      password: password.value,
      firstName: makeFirstLettersBig(firstName.value),
      surname: makeFirstLettersBig(surname.value),
      schoolName: school.value
    };

    this.props.register(body);
  }

  check(property, value) {
    const {checkers} = this.state[property];
    for (let checker of checkers) {
      const errorText = checker(value);
      if (errorText) {
        return errorText
      }
    }
    return '';
  }

  render() {
    const {email, password, firstName, surname, school, registerError} = this.state;
    const hasErrorText = formControlValue => {
      return formControlValue.errorText !== '' &&
        formControlValue.errorText !== undefined &&
        formControlValue.errorText !== null
    };

    const {schoolsNames} = this.props;
    const createSelectItems = schoolName => {
      return <MenuItem value={schoolName} key={schoolName}>{schoolName}</MenuItem>
    };
    const selectItems = schoolsNames.map(createSelectItems);

    const form =
      <form className="register-form">
        <div className="register-form__fields-container">
          <div>
            <TextField
              type="email"
              label="Email"
              name="email"
              error={hasErrorText(email)}
              helperText={hasErrorText(email) ? email.errorText : ''}
              onChange={this.setStateProperty}
              margin="dense"
              required
              fullWidth/>
          </div>
          <div>
            <TextField
              type="password"
              label={<Translate value="password"/>}
              name="password"
              error={hasErrorText(password)}
              helperText={hasErrorText(password) ? password.errorText : ''}
              autoComplete="current-password"
              onChange={this.setStateProperty}
              margin="dense"
              required
              fullWidth/>
          </div>
          <div>
            <TextField
              label={<Translate value="firstName"/>}
              name="firstName"
              error={hasErrorText(firstName)}
              helperText={hasErrorText(firstName) ? firstName.errorText : ''}
              onChange={this.setStateProperty}
              margin="dense"
              required
              fullWidth/>
          </div>
          <div>
            <TextField
              label={<Translate value="surname"/>}
              name="surname"
              error={hasErrorText(surname)}
              helperText={hasErrorText(surname) ? surname.errorText : ''}
              onChange={this.setStateProperty}
              margin="dense"
              required
              fullWidth/>
          </div>
          <FormControl error={hasErrorText(school)} required fullWidth>
            <InputLabel id="school-label"><Translate value="school"/></InputLabel>
            <Select
              labelId="school-label"
              value={school.value}
              name="school"
              onChange={this.setStateProperty}
              displayEmpty>
              {selectItems}
            </Select>
            {hasErrorText(school) ? <FormHelperText>{school.errorText}</FormHelperText> : null}
          </FormControl>
        </div>
        <Button
          onClick={this.register}
          disabled={this.props.pending}
          variant="contained"
          color="primary"
          size="medium"
          fullWidth>
          {this.props.pending ? <Spinner size={24}/> : <Translate value="register"/>}
        </Button>
        {registerError ?
          <FormHelperText error>
            <Translate value="errorText.unableToRegister"/>
          </FormHelperText> :
          null}
      </form>;

    return (
      <ThemeProvider theme={theme}>{form}</ThemeProvider>
    )
  }
}

const mapStateToProps = state => ({
  schoolsNames: state.registerReducer.schoolsNames,
  success: state.registerReducer.successPost,
  error: state.registerReducer.errorRegister,
  pending: state.registerReducer.pending
});

const mapDispatchToProps = {
  getSchoolsNames,
  register,
  reset,
};


export default connect(mapStateToProps, mapDispatchToProps)(RegisterForm);
