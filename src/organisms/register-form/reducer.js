import {types} from './actions';
import {initInterceptor} from "../../utils/tokenConfig";

const initState = {
  pending: false,
  successGet: false,
  successPost: false,
  errorSchools: false,
  errorRegister: false,
  schoolsNames: []
};

export default (state = initState, action) => {
  switch (action.type) {
    case types.GET_SCHOOLS_NAMES_PENDING: {
      return {
        ...state,
        pending: true,
        successGet: false,
        errorSchools: false
      }
    }
    case types.GET_SCHOOLS_NAMES_SUCCESS: {
      return {
        ...state,
        pending: false,
        successGet: true,
        errorSchools: false,
        schoolsNames: action.schoolsNames
      }
    }
    case types.GET_SCHOOLS_NAMES_ERROR: {
      return {
        ...state,
        pending: false,
        successGet: false,
        errorSchools: true
      }
    }
    case types.GET_SCHOOLS_NAMES_FLUSH: {
      return {
        ...state,
        pending: false,
        successGet: false,
        errorSchools: false,
        schoolsNames: []
      }
    }
    case types.REGISTER_PENDING: {
      return {
        ...state,
        successPost: false,
        pending: true,
        errorRegister: false
      };
    }
    case types.REGISTER_SUCCESS: {
      const { token } = action;
      localStorage.setItem('token', token);
      initInterceptor();

      return {
        ...state,
        successPost: true,
        pending: false,
        errorRegister: false
      }
    }
    case types.REGISTER_ERROR: {
      return {
        ...state,
        successPost: false,
        pending: false,
        errorRegister: true
      }
    }
    case types.REGISTER_FLUSH: {
      return {
        ...state,
        successPost: false,
        pending: false,
        errorRegister: false
      }
    }
    default: {
      return state;
    }
  }
}
