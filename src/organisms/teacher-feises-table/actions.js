import {get, to} from "../../utils/apiServiceBase";

export const types = {
  GET_FEISES_PENDING: 'GET_FEISES_PENDING',
  GET_FEISES_SUCCESS: 'GET_FEISES_SUCCESS',
  GET_FEISES_ERROR: 'GET_FEISES_ERROR',
  GET_FEISES_FLUSH: 'GET_FEISES_FLUSH'
};

export const getFeises = () => {
  return async dispatch => {
    dispatch({
      type: types.GET_FEISES_PENDING
    });

    const feisesUrl = '/api/feises/opened';
    const [error, result] = await to(get(feisesUrl));

    if (error) {
      return dispatch({
        type: types.GET_FEISES_ERROR
      })
    }

    return dispatch({
      type: types.GET_FEISES_SUCCESS,
      feises: result.data
    })
  }
};

export const reset = () => {
  return dispatch => {
    dispatch({
      type: types.GET_FEISES_FLUSH
    })
  }
};
