import React, {Component} from "react";
import {connect} from "react-redux";
import {getFeises} from "./actions";
import {Localize, Translate} from "react-redux-i18n";
import {
  Hidden,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TablePagination,
  TableRow,
  Typography
} from "@material-ui/core";
import SearchForm from "../../molecules/search-from/SearchForm";
import {getRegistrationClosingDate} from "../../utils/dateFunctions";
import './TeacherFeisesTable.css';
import Spinner from "../../atoms/spinner/Spinner";
import {getPaginationText} from "../../utils/stringFunctions";

class TeacherFeisesTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      rowsPerPage: 25,
      page: 0,
      filters: {}
    };
    this.changePage = this.changePage.bind(this);
    this.changeRowsPerPage = this.changeRowsPerPage.bind(this);
    this.setFilters = this.setFilters.bind(this);
  }

  componentDidMount() {
    const {getFeises} = this.props;
    getFeises();
  }

  changePage(event, newPage) {
    this.setState({
      page: newPage
    });
  }

  changeRowsPerPage(event) {
    this.setState({
      rowsPerPage: parseInt(event.target.value, 10),
      page: 0
    });
  }

  setFilters({feisName, city, region}) {
    this.setState({
      filters: {
        feisName: feisName.toLowerCase(),
        city: city.toLowerCase(),
        region: region.toLowerCase()
      }
    })
  }

  render() {
    const columnHeadings = [
      'nameOfFeis',
      'date',
      'location',
      'organizer'
    ];
    const createHeadCell = heading => {
      return <TableCell
        key={heading}
        className="teacher-feises-table__head-cell"
        align="center">
        <Translate value={heading}/>
      </TableCell>
    };
    const headCells = columnHeadings.map(createHeadCell);

    const filtersAreSet = () => {
      for (let filter in this.state.filters) {
        if (this.state.filters[filter]) return true;
      }
      return false;
    };

    const filterFeises = feis => {
      const {filters} = this.state;
      for (let property in filters) {
        if (!filters[property]) {
          continue;
        }

        switch (property) {
          case 'feisName': {
            if (!feis.name.toLowerCase().startsWith(filters[property])) return false;
            break;
          }
          case 'region': {
            if (!feis.school.region.toLowerCase().startsWith(filters[property])) return false;
            break;
          }
          case 'city': {
            if (!feis.school.city.toLowerCase().startsWith(filters[property])) return false;
            break;
          }
        }
      }
      return true;
    };

    let feises;
    if (filtersAreSet()) {
      feises = this.props.feises.filter(filterFeises);
    } else {
      feises = this.props.feises;
    }

    const createFeisRow = feis => {
      const startDate = <Localize value={feis.startDate} dateFormat="dateFormat"/>;
      const closingDate = <Localize value={getRegistrationClosingDate(feis)} dateFormat="dateFormat"/>;

      return <TableRow key={feis.id} hover>
        <Hidden mdUp>
          <TableCell>
            <Typography paragraph><b>{feis.name}</b></Typography>
            <Typography>
              <Translate value="date"/> {startDate}
            </Typography>
            <Typography>
              <Translate value="registrationCloses"/> {closingDate}
            </Typography>
            <Typography>{feis.school.city}, {feis.school.region}</Typography>
            <Typography>{feis.school.name}</Typography>
          </TableCell>
        </Hidden>
        <Hidden smDown>
          <TableCell>{feis.name}</TableCell>
          <TableCell>
            {startDate}
            <Typography variant="body2">
              <Translate value="registrationCloses"/> {closingDate}
            </Typography>
          </TableCell>
          <TableCell>{feis.school.city}, {feis.school.region}</TableCell>
          <TableCell>{feis.school.name}</TableCell>
        </Hidden>
      </TableRow>
    };
    const feisesRows = feises.map(createFeisRow);

    const searchFieldsData = [
      {label: 'nameOfFeis', name: 'feisName'},
      {label: 'region', name: 'region'},
      {label: 'city', name: 'city'}
    ];

    const {page, rowsPerPage} = this.state;

    return (
      <div className="teacher-feises-table">
        <SearchForm fieldsData={searchFieldsData} search={this.setFilters}/>
        {this.props.pending ?
          <Spinner size={40}/> :
          <Paper>
            <TablePagination
              component="div"
              count={feisesRows.length}
              page={page}
              rowsPerPage={rowsPerPage}
              rowsPerPageOptions={[25, 50, 100]}
              labelRowsPerPage={<Translate value="rowsPerPage"/>}
              labelDisplayedRows={getPaginationText}
              onChangePage={this.changePage}
              onChangeRowsPerPage={this.changeRowsPerPage}/>
            <TableContainer>
              <Table>
                <TableHead>
                  <TableRow>
                    {headCells}
                  </TableRow>
                </TableHead>
                <TableBody>
                  {feisesRows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)}
                </TableBody>
              </Table>
            </TableContainer>
          </Paper>}
      </div>
    )
  }

}

const mapStateToProps = state => ({
  feises: state.teacherFeisesReducer.feises,
  pending: state.teacherFeisesReducer.pending
});

const mapDispatchToProps = {
  getFeises
};

export default connect(mapStateToProps, mapDispatchToProps)(TeacherFeisesTable);
