import React from "react";
import PropTypes from "prop-types";
import {AppBar, IconButton, makeStyles, Toolbar} from "@material-ui/core";
import ThemeProvider from "@material-ui/styles/ThemeProvider";
import MenuIcon from '@material-ui/icons/Menu';
import Logo from "../../atoms/logo/Logo";
import LangSelect from "../../molecules/lang-select/LangSelect";
import theme from "../../utils/theme";
import './Header.css';

const Header = ({children, onToggleMobilePanel}) => {
  const useStyles = makeStyles({
    toolbar: {
      justifyContent: 'space-between'
    },
    menuButton: {
      marginRight: theme.spacing(2),
      [theme.breakpoints.up('md')]: {
        display: 'none',
      },
    }
  });

  const isAuthorized = () => {
    return localStorage.getItem('token') !== null
  };

  const classes = useStyles();
  const header =
    <AppBar position="static">
      <Toolbar className={classes.toolbar}>
        <div className="header__settings">
          {isAuthorized() ?
            <IconButton
              className={classes.menuButton}
              color="inherit"
              onClick={onToggleMobilePanel}>
              <MenuIcon/>
            </IconButton> :
            null}
          <Logo/>
        </div>
        <div className="header__settings">
          <LangSelect/>
          {children}
        </div>
      </Toolbar>
    </AppBar>;

  return (
    <ThemeProvider theme={theme}>{header}</ThemeProvider>
  );
};

Header.propTypes = {
  onToggleMobilePanel: PropTypes.func,
};

export default Header;
