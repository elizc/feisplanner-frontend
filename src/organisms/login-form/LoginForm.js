import React, {Component} from "react";
import {connect} from "react-redux";
import {Button, FormHelperText, TextField, ThemeProvider} from "@material-ui/core";
import {Translate} from 'react-redux-i18n';
import theme from "../../utils/theme";
import {exists} from "../../utils/formCheckers";
import {isEqual} from "../../utils/comparator";
import {roles} from "../../utils/constants";
import history from "../../utils/history";
import * as paths from '../../utils/paths';
import {login, reset} from "./actions";
import './LoginForm.css';
import Spinner from "../../atoms/spinner/Spinner";

class LoginForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loginError: false,
      email: {
        value: '',
        errorText: '',
        checkers: [exists]
      },
      password: {
        value: '',
        errorText: '',
        checkers: [exists]
      }
    };
    this.setStateProperty = this.setStateProperty.bind(this);
    this.login = this.login.bind(this);
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (this.props.success) {
      const {userRole} = this.props;
      if (userRole === roles.ROLE_ADMIN) {
        history.push(paths.SCHOOLS);
      } else {
        history.push(paths.STUDENTS);
      }
      this.props.reset();
    }
    if (!isEqual(prevProps, this.props) && this.props.error) {
      this.setState({
        loginError: true
      });
    }
  }


  setStateProperty(event) {
    const {target} = event;
    const property = target.name;
    const value = target.value;

    this.setState(prevState => ({
      loginError: false,
      [property]: {
        ...prevState[property],
        value
      }
    }));
  }

  login(event) {
    const getErrorsCount = () => {
      let errorsCount = 0;
      for (let property in this.state) {
        if (!this.state[property].hasOwnProperty('checkers')) continue;

        const {value} = this.state[property];
        const errorText = this.check(property, value);
        if (errorText) {
          errorsCount++;
          this.setState({
            [property]: {
              ...this.state[property],
              errorText
            }
          })
        } else {
          this.setState({
            [property]: {
              ...this.state[property],
              errorText: '',
            }
          })
        }
      }
      return errorsCount;
    };

    event.preventDefault();
    if (getErrorsCount() !== 0) {
      return false;
    }

    const {email, password} = this.state;
    this.props.login(email.value, password.value);
  }

  check(property, value) {
    const {checkers} = this.state[property];
    for (let checker of checkers) {
      const errorText = checker(value);
      if (errorText) {
        return errorText
      }
    }
    return '';
  }

  render() {
    const {email, password, loginError} = this.state;
    const hasErrorText = formControlValue => {
      return formControlValue.errorText !== '' &&
        formControlValue.errorText !== undefined &&
        formControlValue.errorText !== null
    };

    const form =
      <form className="login-form">
        <div className="login-form__fields-container">
          <div>
            <TextField
              type="email"
              label="Email"
              name="email"
              error={hasErrorText(email)}
              helperText={hasErrorText(email) ? email.errorText : ''}
              onChange={this.setStateProperty}
              margin="dense"
              required
              fullWidth/>
          </div>
          <div>
            <TextField
              type="password"
              label={<Translate value="password"/>}
              name="password"
              error={hasErrorText(password)}
              helperText={hasErrorText(password) ? password.errorText : ''}
              autoComplete="current-password"
              onChange={this.setStateProperty}
              margin="dense"
              required
              fullWidth/>
          </div>
        </div>
        <Button
          onClick={this.login}
          disabled={this.props.pending}
          variant="contained"
          color="primary"
          size="medium"
          fullWidth>
          {this.props.pending ? <Spinner size={24}/> : <Translate value="logIn"/>}
        </Button>
        {loginError ?
          <FormHelperText error>
            <Translate value="errorText.wrongCredentials"/>
          </FormHelperText> :
          null}
      </form>;

    return (
      <ThemeProvider theme={theme}>{form}</ThemeProvider>
    )
  }
}

const mapStateToProps = state => ({
  success: state.loginReducer.success,
  error: state.loginReducer.error,
  pending: state.loginReducer.pending,
  userRole: state.loginReducer.userRole
});

const mapDispatchToProps = {
  login,
  reset
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginForm);
