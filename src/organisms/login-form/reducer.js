import {types} from "./actions";
import {constants} from "../../utils/constants";
import * as jwt from "jsonwebtoken";
import {initInterceptor} from "../../utils/tokenConfig";

const initState = {
  success: false,
  pending: false,
  error: false,
  userRole: ''
};

export default (state = initState, action) => {
  switch (action.type) {
    case types.LOGIN_PENDING: {
      return {
        ...state,
        success: false,
        pending: true,
        error: false
      }
    }
    case types.LOGIN_SUCCESS: {
      const { token } = action;
      localStorage.setItem('token', token);
      const parsedToken = jwt.verify(token, constants.JWT_KEY);
      initInterceptor();

      return {
        ...state,
        success: true,
        pending: false,
        error: false,
        userRole: parsedToken[constants.USER_ROLE_KEY]
      }
    }
    case types.LOGIN_ERROR: {
      return {
        ...state,
        success: false,
        pending: false,
        error: true
      }
    }
    case types.LOGIN_FLUSH: {
      return {
        ...state,
        success: false,
        pending: false,
        error: false
      }
    }
    default: {
      return state;
    }
  }
}
