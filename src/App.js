import React from 'react';
import {Provider} from "react-redux";
import {Redirect, Route, Router, Switch} from 'react-router-dom';
import AuthPage from "./pages/auth-page/AuthPage";
import SchoolsPage from "./pages/schools-page/SchoolsPage";
import history from "./utils/history";
import * as paths from './utils/paths';
import {checkUserRole, initInterceptor, isAuthenticated} from "./utils/tokenConfig";
import {roles} from "./utils/constants";
import store from "./store";
import StudentsPage from "./pages/students-page/StudentsPage";
import AdminFeisesPage from "./pages/admin-feises-page/AdminFeisesPage";
import TeacherFeisesPage from "./pages/teacher-feises-page/TeacherFeisesPage";
import TeacherEntriesPage from "./pages/teacher-entries-page/TeacherEntriesPage";
import OrgEntriesPage from "./pages/org-entries-page/OrgEntriesPage";
import TimetablePage from "./pages/timetable-page/TimetablePage";
import DocsPage from "./pages/docs-page/DocsPage";
import AgeGroupsPage from "./pages/age-groups-page/AgeGroupsPage";
import './App.css';
import 'typeface-roboto';

if (isAuthenticated()) {
  initInterceptor();
}

const checkRole = (node, role) => {
  return checkUserRole(role) ? node : <Redirect to={paths.FORBIDDEN}/>
};

const checkAuth = node => {
  return isAuthenticated() ? node : <Redirect to={paths.LOGIN}/>
};


function App() {

  return (
    <Provider store={store}>
      <Router history={history}>
        <Switch>
          <Route exact path={paths.LOGIN} render={() => <AuthPage/>}/>
          <Route exact path={paths.ROOT} render={() => <Redirect to={paths.LOGIN}/>}/>
          <Route exact path={paths.STUDENTS} render={() => checkAuth(<StudentsPage/>)}/>
          <Route exact path={paths.OPENED_FEISES} render={() => checkAuth(<TeacherFeisesPage/>)}/>
          <Route exact path={paths.TEACHER_ENTRIES} render={() => checkAuth(<TeacherEntriesPage/>)}/>
          <Route exact path={paths.ORG_ENTRIES} render={() => checkAuth(<OrgEntriesPage/>)}/>
          <Route exact path={paths.TIMETABLE} render={() => checkAuth(<TimetablePage/>)}/>
          <Route exact path={paths.DOCS} render={() => checkAuth(<DocsPage/>)}/>
          <Route exact path={paths.AGE_GROUPS} render={() => checkAuth(<AgeGroupsPage/>)}/>
          <Route exact path={paths.SCHOOLS} render={() => checkRole(<SchoolsPage/>, roles.ROLE_ADMIN)}/>
          <Route exact path={paths.UNFINISHED_FEISES} render={() => checkRole(<AdminFeisesPage/>, roles.ROLE_ADMIN)}/>
        </Switch>
      </Router>
    </Provider>
  );
}

export default App;
