import React, {Fragment} from "react";
import {CssBaseline, Typography} from "@material-ui/core";
import {Translate} from "react-redux-i18n";
import MainNav from "../../organisms/main-nav/MainNav";
import TeacherFeisesTable from "../../organisms/teacher-feises-table/TeacherFeisesTable";
import './TeacherFeisesPage.css';

const TeacherFeisesPage = () => {
  return (
    <Fragment>
      <CssBaseline/>
      <MainNav/>
      <main className="teacher-feises-page">
        <Typography
          component="h1"
          variant="h4"
          align="center"
          paragraph>
          <Translate value="feises"/>
        </Typography>
        <TeacherFeisesTable/>
      </main>
    </Fragment>
  )
};

export default TeacherFeisesPage;
