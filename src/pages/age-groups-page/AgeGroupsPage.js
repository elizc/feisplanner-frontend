import React, {Fragment} from "react";
import {CssBaseline, Typography} from "@material-ui/core";
import {Translate} from "react-redux-i18n";
import MainNav from "../../organisms/main-nav/MainNav";
import './AgGroupsPage.css';
import AgeGroupsList from "../../organisms/age-groups-list/AgeGroupsList";

const AgeGroupsPage = () => {
  return (
    <Fragment>
      <CssBaseline/>
      <MainNav/>
      <main className="age-groups-page">
        <Typography
          component="h1"
          variant="h4"
          align="center"
          paragraph>
          <Translate value="ageGroups"/>
        </Typography>
        <AgeGroupsList/>
      </main>
    </Fragment>
  )
};

export default AgeGroupsPage;
