import React, {Fragment} from 'react';
import {CssBaseline, Typography} from "@material-ui/core";
import {Translate} from 'react-redux-i18n';
import MainNav from "../../organisms/main-nav/MainNav";
import SchoolsTable from "../../organisms/schools-table/SchoolsTable";
import './SchoolsPage.css';

const SchoolsPage = () => {
  return (
    <Fragment>
      <CssBaseline/>
      <MainNav/>
      <main className="schools-page">
        <Typography
          component="h1"
          variant="h4"
          align="center"
          paragraph>
          <Translate value="schools"/>
        </Typography>
        <SchoolsTable/>
      </main>
    </Fragment>
  )
};

export default SchoolsPage;
