import React, {Component, Fragment} from "react";
import {connect} from "react-redux";
import {
  Backdrop,
  CssBaseline,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Paper,
  Typography
} from "@material-ui/core";
import {
  CheckCircleOutline,
  FormatListNumberedOutlined,
  ListAlt,
  Money,
  NoteOutlined,
  PeopleAltOutlined
} from "@material-ui/icons";
import {ThemeProvider} from "@material-ui/styles";
import {Translate} from "react-redux-i18n";
import MainNav from "../../organisms/main-nav/MainNav";
import theme from "../../utils/theme";
import {getGeneratedDoc, getJudgesSheets} from "./actions";
import Spinner from "../../atoms/spinner/Spinner";
import './DocsPage.css';

class DocsPage extends Component {
  constructor(props) {
    super(props);
    this.downloadDancersReport = this.downloadDancersReport.bind(this);
    this.downloadScoringTable = this.downloadScoringTable.bind(this);
    this.downloadStageLists = this.downloadStageLists.bind(this);
    this.downloadNumbers = this.downloadNumbers.bind(this);
    this.downloadStickers= this.downloadStickers.bind(this);
    this.downloadJudgesSheets = this.downloadJudgesSheets.bind(this);
  }

  downloadDancersReport() {
    const {getGeneratedDoc} = this.props;
    getGeneratedDoc("all-dancers");
  }

  downloadScoringTable() {
    const {getGeneratedDoc} = this.props;
    getGeneratedDoc("scoring-table");
  }

  downloadStageLists() {
    const {getGeneratedDoc} = this.props;
    getGeneratedDoc("stage-lists");
  }

  downloadNumbers() {
    const {getGeneratedDoc} = this.props;
    getGeneratedDoc("numbers");
  }

  downloadStickers() {
    const {getGeneratedDoc} = this.props;
    getGeneratedDoc("stickers");
  }

  downloadJudgesSheets() {
    const {getJudgesSheets} = this.props;
    getJudgesSheets();
  }

  render() {
    const list = <List component={Paper} className="docs-page__list">
      <ListItem onClick={this.downloadDancersReport} divider button>
        <ListItemIcon>
          <PeopleAltOutlined color="primary"/>
        </ListItemIcon>
        <ListItemText primary={<Translate value="allDancersReport"/>}/>
      </ListItem>
      <ListItem onClick={this.downloadScoringTable} divider button>
        <ListItemIcon>
          <FormatListNumberedOutlined color="primary"/>
        </ListItemIcon>
        <ListItemText primary={<Translate value="scoringTables"/>}/>
      </ListItem>
      <ListItem onClick={this.downloadStageLists} divider button>
        <ListItemIcon>
          <CheckCircleOutline color="primary"/>
        </ListItemIcon>
        <ListItemText primary={<Translate value="stageLists"/>}/>
      </ListItem>
      <ListItem onClick={this.downloadJudgesSheets} divider button>
        <ListItemIcon>
          <ListAlt color="primary"/>
        </ListItemIcon>
        <ListItemText primary={<Translate value="judgesSheets"/>}/>
      </ListItem>
      <ListItem onClick={this.downloadNumbers} divider button>
        <ListItemIcon>
          <Money color="primary"/>
        </ListItemIcon>
        <ListItemText primary={<Translate value="numbers"/>}/>
      </ListItem>
      <ListItem onClick={this.downloadStickers} button>
        <ListItemIcon>
          <NoteOutlined color="primary"/>
        </ListItemIcon>
        <ListItemText primary={<Translate value="stickersForNumbers"/>}/>
      </ListItem>
    </List>;

    const backdropStyles = {
      backgroundColor: 'rgba(255, 255, 255, 0.3)',
      zIndex: 4000
    }

    return (
      <Fragment>
        <CssBaseline/>
        <MainNav/>
        <main className="docs-page">
          <Typography
            component="h1"
            variant="h4"
            align="center"
            paragraph>
            <Translate value="documentation"/>
          </Typography>
          <ThemeProvider theme={theme}>{list}</ThemeProvider>
        </main>
        <Backdrop open={this.props.pending} style={backdropStyles} timeout={300}>
          <Spinner size={40}/>
        </Backdrop>
      </Fragment>
    )
  }
}

const mapStateToProps = (state) => ({
  error: state.docReducer.error,
  pending: state.docReducer.pending
});

const mapDispatchToProps = {
  getGeneratedDoc,
  getJudgesSheets
};

export default connect(mapStateToProps, mapDispatchToProps)(DocsPage);

