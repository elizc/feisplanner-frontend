import {types} from "./actions";

const initState = {
  pending: false,
  success: false,
  error: false
};

export default (state = initState, action) => {
  switch (action.type) {
    case types.DOC_PENDING: {
      return {
        ...state,
        pending: true,
        error: false,
        success: false
      }
    }
    case types.DOC_SUCCESS_LOAD: {
      return {
        ...state,
        pending: false,
        error: false,
        success: true
      }
    }
    case types.DOC_SUCCESS_NO_LOAD: {
      return {
        ...state,
        pending: false,
        error: false,
        success: true
      }
    }
    case types.DOC_ERROR: {
      return {
        ...state,
        pending: false,
        success: false,
        error: true
      }
    }
    default: {
      return state;
    }
  }
};

