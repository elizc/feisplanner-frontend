import history from "../../utils/history";
import {get, to} from "../../utils/apiServiceBase";
import {getFeisIdFromOrgUrl} from "../../utils/stringFunctions";

export const types = {
  DOC_PENDING: 'DOC_PENDING',
  DOC_SUCCESS_NO_LOAD: 'DOC_SUCCESS_NO_LOAD',
  DOC_ERROR: 'DOC_ERROR',
  DOC_SUCCESS_LOAD: 'DOC_SUCCESS_LOAD'
};

export const getGeneratedDoc = docPathVariable => {
  return async dispatch => {
    dispatch({
      type: types.DOC_PENDING
    });

    const {pathname} = history.location;
    const feisId = getFeisIdFromOrgUrl(pathname);
    const url = `/api/docs/${docPathVariable}?feis=${feisId}`;
    let [error, result] = await to(get(url));

    if (error) {
      return dispatch({
        type: types.DOC_ERROR
      })
    }

    const { data, status } = result;
    if (status === 204) {
      return dispatch({
        type: types.DOC_SUCCESS_NO_LOAD
      })
    }

    window.open(data, "_blank");
    return dispatch({
      type: types.DOC_SUCCESS_LOAD
    })
  }
};

export const getJudgesSheets = () => {
  return () => {
    const url = `/download/judges_sheets.pdf`;
    window.open(url, "_blank");
  }
};
