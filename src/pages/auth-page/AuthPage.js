import React, {Component, Fragment} from "react";
import {CssBaseline, Paper, Tab, Tabs, Typography} from "@material-ui/core";
import ThemeProvider from "@material-ui/styles/ThemeProvider";
import {Translate} from "react-redux-i18n";
import Header from "../../organisms/header/Header";
import RegisterForm from "../../organisms/register-form/RegisterForm";
import LoginForm from "../../organisms/login-form/LoginForm";
import TabPanel from "../../organisms/tab-panel/TabPanel";
import theme from "../../utils/theme";
import './AuthPage.css';

class AuthPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tabValue: 0
    };
    this.changeTab = this.changeTab.bind(this);
  }

  changeTab(event, newValue) {
    this.setState({
      tabValue: newValue
    })
  }

  render() {
    const {tabValue} = this.state;
    const sidebar =
      <Paper square>
        <Tabs value={tabValue} onChange={this.changeTab} indicatorColor="primary" textColor="primary" centered>
          <Tab className="auth-page__tab" label={<Translate value="loggingIn"/>} value={0}/>
          <Tab className="auth-page__tab" label={<Translate value="registration"/>} value={1}/>
        </Tabs>
      </Paper>;

    return (
      <Fragment>
        <CssBaseline/>
        <Header/>
        <div className="auth-page">

          <main className="auth-page__app-description">
            <Typography
              variant="h4"
              component="h1"
              align="center"
              paragraph>
              <Translate value="appDescription.heading"/>
            </Typography>
            <div className="auth-page__photo"/>
            <Typography paragraph><Translate value="appDescription.howToLogIn"/></Typography>
            <Typography><Translate value="appDescription.howToRegister"/></Typography>
          </main>
          <aside className="auth-page__form">
            <ThemeProvider theme={theme}>{sidebar}</ThemeProvider>
            <TabPanel value={tabValue} index={0}>
              <LoginForm/>
            </TabPanel>
            <TabPanel value={tabValue} index={1}>
              <RegisterForm/>
            </TabPanel>
          </aside>
        </div>
      </Fragment>
    )
  }
}

export default AuthPage;
