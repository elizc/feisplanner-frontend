import React, {Fragment} from 'react';
import {CssBaseline, Typography} from "@material-ui/core";
import {Translate} from "react-redux-i18n";
import MainNav from "../../organisms/main-nav/MainNav";
import StudentsTable from "../../organisms/students-table/StudentsTable";
import './StudentsPage.css';

const StudentsPage = () => {
  return(
    <Fragment>
      <CssBaseline/>
      <MainNav/>
      <main className="students-page">
        <Typography
          component="h1"
          variant="h4"
          align="center"
          paragraph>
          <Translate value="students"/>
        </Typography>
        <StudentsTable/>
      </main>
    </Fragment>
  )
};

export default StudentsPage;
