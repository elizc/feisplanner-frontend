import React, {Fragment} from "react";
import {CssBaseline, Typography} from "@material-ui/core";
import {Translate} from "react-redux-i18n";
import MainNav from "../../organisms/main-nav/MainNav";
import TeacherEntriesTable from "../../organisms/teacher-entries-table/TeacherEntriesTable";
import './TeacherEntriesPage.css';

const TeacherEntriesPage = () => {
  return (
    <Fragment>
      <CssBaseline/>
      <MainNav/>
      <main className="teacher-entries-page">
        <Typography
          component="h1"
          variant="h4"
          align="center"
          paragraph>
          <Translate value="entries"/>
        </Typography>
        <TeacherEntriesTable/>
      </main>
    </Fragment>
  )
};

export default TeacherEntriesPage;
