import React, {Fragment} from "react";
import {CssBaseline, Typography} from "@material-ui/core";
import MainNav from "../../organisms/main-nav/MainNav";
import {Translate} from "react-redux-i18n";
import TimetableForm from "../../organisms/timetable-form/TimetableForm";
import './TimetablePage.css';

const TimetablePage = () => {
  return (
    <Fragment>
      <CssBaseline/>
      <MainNav/>
      <main className="timetable-page">
        <Typography
          component="h1"
          variant="h4"
          align="center"
          paragraph>
          <Translate value="timetable"/>
        </Typography>
        <TimetableForm/>
      </main>
    </Fragment>
  )
};

export default TimetablePage;
