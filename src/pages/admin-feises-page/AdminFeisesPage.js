import React, {Fragment} from 'react';
import {CssBaseline, Typography} from "@material-ui/core";
import {Translate} from 'react-redux-i18n';
import MainNav from "../../organisms/main-nav/MainNav";
import FeisesTable from "../../organisms/admin-feises-table/AdminFeisesTable";
import './AdminFeisesPage.css';

const AdminFeisesPage = () => {
  return (
    <Fragment>
      <CssBaseline/>
      <MainNav/>
      <main className="admin-feises-page">
        <Typography
          component="h1"
          variant="h4"
          align="center"
          paragraph>
          <Translate value="feises"/>
        </Typography>
        <FeisesTable/>
      </main>
    </Fragment>
  )
};

export default AdminFeisesPage;
