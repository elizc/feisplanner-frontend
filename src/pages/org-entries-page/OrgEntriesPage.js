import React, {Fragment} from "react";
import {CssBaseline, Typography} from "@material-ui/core";
import MainNav from "../../organisms/main-nav/MainNav";
import {Translate} from "react-redux-i18n";
import './OrgEntriesPage.css';
import OrgEntriesTable from "../../organisms/org-entries-table/OrgEntriesTable";

const OrgEntriesPage = () => {
  return (
    <Fragment>
      <CssBaseline/>
      <MainNav/>
      <main className="org-entries-page">
        <Typography
          component="h1"
          variant="h4"
          align="center"
          paragraph>
          <Translate value="entries"/>
        </Typography>
        <OrgEntriesTable/>
      </main>
    </Fragment>
  )
};

export default OrgEntriesPage;
