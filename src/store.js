import {applyMiddleware, createStore} from "redux";
import thunk from "redux-thunk";
import {loadTranslations, setLocale, syncTranslationWithStore} from "react-redux-i18n";
import dictionary from "./utils/dictionary";
import appReducer from "./appReducer";
import {getLang} from "./utils/stringFunctions";

const store = createStore(appReducer, applyMiddleware(thunk));
syncTranslationWithStore(store);
store.dispatch(loadTranslations(dictionary));
store.dispatch(setLocale(getLang()));

export default store;
