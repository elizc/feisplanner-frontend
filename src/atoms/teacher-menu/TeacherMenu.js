import React, {Component, Fragment} from "react";
import {connect} from "react-redux";
import {Translate} from "react-redux-i18n";
import {Link} from "react-router-dom";
import {Collapse, Divider, List, ListItem, ListItemText} from "@material-ui/core";
import {ExpandLess, ExpandMore} from "@material-ui/icons";
import * as jwt from "jsonwebtoken";
import {constants} from "../../utils/constants";
import {getFeises} from "./actions";
import './TeacherMenu.css';

class TeacherMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      openTeacherSubmenu: true,
      openOrgSubmenu: true
    };
    this.openTeacherSubmenu = this.openTeacherSubmenu.bind(this);
    this.openOrgSubmenu = this.openOrgSubmenu.bind(this);
  }

  componentDidMount() {
    const {getFeises} = this.props;
    const token = localStorage.getItem('token');
    const parsedToken = jwt.verify(token, constants.JWT_KEY);
    const userId = parsedToken[constants.USER_ID_KEY];
    getFeises(userId);
  }

  openTeacherSubmenu() {
    this.setState(prevState => ({
      openTeacherSubmenu: !prevState.openTeacherSubmenu
    }))
  }

  openOrgSubmenu() {
    this.setState(prevState => ({
      openOrgSubmenu: !prevState.openOrgSubmenu
    }))
  }

  render() {
    const {openTeacherSubmenu, openOrgSubmenu} = this.state;
    const {feises} = this.props;

    const createTeacherSubmenu = () => {
      const menuItemsData = [
        {text: 'students', link: '/students'},
        {text: 'feises', link: '/feises'},
        {text: 'entries', link: '/entries'}
      ];
      const createMenuItem = data => {
        const itemStyle = {paddingLeft: '40px'};
        return <ListItem style={itemStyle} key={data.text} button>
          <Link className="teacher-menu__link" to={data.link}>
            <ListItemText primary={<Translate value={data.text}/>}/>
          </Link>
        </ListItem>
      };

      return menuItemsData.map(createMenuItem);
    };

    const createOrgSubmenu = feises => {
      const createMenuItem = feis => {
        const outerItemStyle = {paddingLeft: '40px'};
        const menuItemsData = [
          {text: 'entries', link: `/entries/${feis.id}`},
          {text: 'ageGroups', link: `/age-groups/${feis.id}`},
          {text: 'timetable', link: `/timetable/${feis.id}`},
          {text: 'documentation', link: `/documentation/${feis.id}`}
        ];

        const innerMenuItems = menuItemsData.map(data => {
          const innerItemStyle = {paddingLeft: '60px'};
          return <ListItem style={innerItemStyle} key={data.text} button>
            <Link className="teacher-menu__link" to={data.link}>
              <ListItemText primary={<Translate value={data.text}/>}/>
            </Link>
          </ListItem>
        });

        return <Fragment key={feis.id}>
          <ListItem style={outerItemStyle}>
            <ListItemText primary={feis.name}/>
          </ListItem>
          <List component="div" disablePadding>
            {innerMenuItems}
          </List>
        </Fragment>
      };

      return feises.map(createMenuItem);
    };

    const teacherMenuItems = createTeacherSubmenu();
    const orgMenuItems = createOrgSubmenu(feises);

    return (
      <Fragment>
        <Divider/>
        <List component="nav" className="teacher-menu">
          <ListItem onClick={this.openTeacherSubmenu} button>
            <ListItemText primary={<Translate value="teacher"/>}/>
            {openTeacherSubmenu ? <ExpandLess/> : <ExpandMore/>}
          </ListItem>
          <Collapse in={openTeacherSubmenu} timeout="auto" unmountOnExit>
            <List component="div" disablePadding>
              {teacherMenuItems}
            </List>
          </Collapse>
          {feises.length !== 0 ?
            <Fragment>
              <ListItem onClick={this.openOrgSubmenu} button>
                <ListItemText primary={<Translate value="organizer"/>}/>
                {openOrgSubmenu ? <ExpandLess/> : <ExpandMore/>}
              </ListItem>
              <Collapse in={openOrgSubmenu} timeout="auto" unmountOnExit>
                <List component="div" disablePadding>
                  {orgMenuItems}
                </List>
              </Collapse>
            </Fragment>
            : null}
        </List>
      </Fragment>
    )
  }
}

const mapStateToProps = state => ({
  feises: state.teacherMenuReducer.feises
});

const mapDispatchToProps = {
  getFeises: getFeises
};

export default connect(mapStateToProps, mapDispatchToProps)(TeacherMenu);
