import {types} from "./actions";

const initState = {
  pending: false,
  success: false,
  error: false,
  feises: []
};

export default (state = initState, action) => {
  switch (action.type) {
    case types.GET_FEISES_PENDING: {
      return {
        ...state,
        pending: true,
        success: false,
        error: false
      }
    }
    case types.GET_FEISES_SUCCESS: {
      return {
        ...state,
        pending: false,
        success: true,
        error: false,
        feises: action.feises
      }
    }
    case types.GET_FEISES_ERROR: {
      return {
        ...state,
        pending: false,
        success: false,
        error: true
      }
    }
    case types.GET_FEISES_FLUSH: {
      return {
        ...state,
        pending: false,
        success: false,
        error: false,
        feises: []
      }
    }
    default: {
      return state;
    }
  }
}
