import React from 'react';
import PropTypes from 'prop-types';
import {IconButton} from "@material-ui/core";
import {Delete} from "@material-ui/icons";
import theme from "../../utils/theme";

const DeleteButton = ({onClick, deletedId}) => {
  const iconStyle = {
    color: theme.palette.primary.main
  };

  const buttonStyle = {
    padding: '5px'
  };

  const handleClick = () => {
    onClick(deletedId);
  };

  return(
    <IconButton style={buttonStyle} onClick={handleClick}>
      <Delete style={iconStyle} fontSize="small"/>
    </IconButton>
  )
};

DeleteButton.propTypes = {
  onClick: PropTypes.func.isRequired,
  deletedId: PropTypes.string.isRequired,
};

export default DeleteButton;
