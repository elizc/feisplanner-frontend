import React from 'react';
import PropTypes from 'prop-types';
import {IconButton} from "@material-ui/core";
import {Edit} from "@material-ui/icons";
import theme from '../../utils/theme';

const EditButton = ({onClick, editedEntity}) => {
  const iconStyle = {
    color: theme.palette.primary.main
  };

  const buttonStyle = {
    padding: '5px'
  };

  const handleClick = () => {
    onClick(editedEntity);
  };

  return(
    <IconButton style={buttonStyle} onClick={handleClick}>
      <Edit style={iconStyle} fontSize="small"/>
    </IconButton>
  )
};

EditButton.propTypes = {
  onClick: PropTypes.func.isRequired,
  editedEntity: PropTypes.object.isRequired,
};

export default EditButton;
