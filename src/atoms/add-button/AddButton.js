import React from "react";
import PropTypes from 'prop-types';
import {Button} from "@material-ui/core";
import {Translate} from "react-redux-i18n";
import ThemeProvider from "@material-ui/styles/ThemeProvider";
import theme from "../../utils/theme";

export const AddButton = ({onClick}) => {
  const addButtonStyle = {
    position: 'absolute',
    right: '0',
    top: '-10px',
  };
  const button = <Button
    style={addButtonStyle}
    variant="contained"
    color="primary"
    size="small"
    onClick={onClick}>
    <Translate value="add"/>
  </Button>;
  return <ThemeProvider theme={theme}>{button}</ThemeProvider>
};

AddButton.propTypes = {
  onClick: PropTypes.func.isRequired,
};

export default AddButton;
