import React from "react";
import PropTypes from 'prop-types';
import {Fab} from "@material-ui/core";
import AddIcon from "@material-ui/icons/Add";
import ThemeProvider from "@material-ui/styles/ThemeProvider";
import theme from "../../utils/theme";

const AddFab = ({onClick}) => {
  const fabStyle = {
    position: 'fixed',
    right: '20px',
    bottom: '20px',
    zIndex: '10'
  };
  const fab = <Fab
    style={fabStyle}
    color="primary"
    size="medium"
    onClick={onClick}>
    <AddIcon/>
  </Fab>;
  return <ThemeProvider theme={theme}>{fab}</ThemeProvider>
};

AddFab.propTypes = {
  onClick: PropTypes.func.isRequired
};

export default AddFab;
