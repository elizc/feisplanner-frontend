import React, {Component, Fragment} from 'react';
import {List, Collapse, ListItem, ListItemText, Divider} from "@material-ui/core";
import {ExpandLess, ExpandMore} from "@material-ui/icons";
import {Link} from "react-router-dom";
import {Translate} from 'react-redux-i18n';
import './AdminMenu.css';

class AdminMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: true
    };
    this.openSubmenu = this.openSubmenu.bind(this);
  }

  openSubmenu() {
    this.setState(prevState => ({
      open: !prevState.open
    }))
  }

  render() {
    const {open} = this.state;

    const menuItemsData = [
      {text: 'schools', link: '/schools'},
      {text: 'feises', link: '/unfinished-feises'},
    ];
    const createMenuItem = data => {
      const itemStyle = {paddingLeft: '40px'};
      return <ListItem style={itemStyle} key={data.text} button>
        <Link className="admin-menu__link" to={data.link}><ListItemText primary={
          <Translate value={data.text}/>
        }/></Link>
      </ListItem>
    };
    const menuItems = menuItemsData.map(createMenuItem);

    return (
      <Fragment>
        <Divider/>
        <List component="nav" className="admin-menu">
          <ListItem onClick={this.openSubmenu} button>
            <ListItemText primary={<Translate value="administrator"/>}/>
            {open ? <ExpandLess/> : <ExpandMore/>}
          </ListItem>
          <Collapse in={open} timeout="auto" unmountOnExit>
            <List component="div" disablePadding>
              {menuItems}
            </List>
          </Collapse>
        </List>
      </Fragment>
    )
  }
}

export default AdminMenu;
