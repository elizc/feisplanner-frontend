import React from 'react';
import PropTypes from 'prop-types';
import './LangItem.css';

const LangItem = ({lang, flag, onClick}) => {
  const selectLang = () => {
    onClick(lang.code);
  };

  return (
    <div className="lang-item" onClick={selectLang}>
      <img className="lang-item__flag" src={flag} alt="flag"/>
      {lang.nativeName}
    </div>
  )
};

LangItem.propTypes = {
  lang: PropTypes.shape({
    code: PropTypes.string.isRequired,
    nativeName: PropTypes.string.isRequired,
  }).isRequired,
  flag: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
};

export default LangItem;
