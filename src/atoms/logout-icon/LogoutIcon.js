import React from 'react';
import {IconButton, makeStyles} from "@material-ui/core";
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import history from "../../utils/history";
import * as paths from "../../utils/paths";

const LogoutIcon = () => {
  const logout = () => {
    localStorage.removeItem('token');
    history.push(paths.LOGIN);
  };

  const useStyles = makeStyles({
    root: {
      paddingLeft: 0
    }
  });
  const classes = useStyles();

  return(
    <IconButton
      className={classes.root}
      color="inherit"
      onClick={logout}>
      <ExitToAppIcon/>
    </IconButton>
  )
};

export default LogoutIcon;
