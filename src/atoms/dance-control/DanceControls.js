import React, {Component} from "react";
import PropTypes from 'prop-types';
import {FormControl, FormGroup, FormControlLabel, Checkbox, Typography} from "@material-ui/core";
import {average, hasAtLeast} from "../../utils/arrayFunctions";
import './DanceControls.css';

class DanceControls extends Component {
  render() {
    const labels = {
      'reel': 'Reel',
      'light': 'Light Jig',
      'slip': 'Slip Jig',
      'single': 'Single Jig',
      'treble': 'Treble Jig',
      'hornpipe': 'Hornpipe',
      'tradSet': 'Trad. Set'
    };

    const createCheckbox = (danceName, danceLevel) => {
      let checkBoxValue = false;
      if (soloDances) {
        checkBoxValue = soloDances.find(soloDance => {
          return soloDance === `${danceName} ${danceLevel}`;
        }) !== undefined;
        return <Checkbox
          checked={checkBoxValue}
          onChange={setDance}
          name={`${danceName} ${danceLevel}`}
          color="primary"/>
      } else {
        return <Checkbox
          onChange={setDance}
          name={`${danceName} ${danceLevel}`}
          color="primary"/>
      }
    };

    const createCupDanceItem = student => {
      const canDancePrelim = (softLevels, hardLevels) => {
        const isOpen = level => level === 'Open';
        const openSoftLevels = softLevels.filter(isOpen);
        const openHardLevels = hardLevels.filter(isOpen);
        return openSoftLevels.length >= 2 && openHardLevels.length >= 2;
      };

      const {reelLevel, lightLevel, slipLevel, singleLevel, trebleLevel, hornpipeLevel, tradSetLevel, wonPrelimsCount} = student;
      let danceName = 'Premiership';
      let danceLevel;

      const levelNumbers = {
        'Beginner': 1,
        'Primary': 2,
        'Intermediate': 3,
        'Open': 4
      };
      const premiershipDanceLevels = [levelNumbers[reelLevel], levelNumbers[lightLevel], levelNumbers[trebleLevel]];

      if (hasAtLeast(premiershipDanceLevels, levelNumbers['Beginner'], 2)) {
        danceLevel = 'Beginner';
      }

      if (hasAtLeast(premiershipDanceLevels, levelNumbers['Primary'], 2)
        || Math.round(average(premiershipDanceLevels)) === levelNumbers['Primary']) {
        danceLevel = 'Primary';
      }

      if (hasAtLeast(premiershipDanceLevels, levelNumbers['Intermediate'], 3)
        || Math.round(average(premiershipDanceLevels)) >= levelNumbers['Intermediate']) {
        danceLevel = 'Intermediate';
      }

      if (canDancePrelim(
        [reelLevel, lightLevel, slipLevel, singleLevel],
        [trebleLevel, hornpipeLevel, tradSetLevel]
      )) {
        danceLevel = 'Open';
        danceName = 'Preliminary';
      }

      if (wonPrelimsCount === 2) {
        danceLevel = 'Open';
        danceName = 'Championship';
      }

      return <div className="dance-controls__item" key={`${danceName} ${danceLevel}`}>
        <FormControl>
          <Typography>{danceName}</Typography>
          <FormGroup row>
            <FormControlLabel
              control={createCheckbox(danceName, danceLevel)}
              label={danceLevel}/>
          </FormGroup>
        </FormControl>
      </div>
    };

    const {student, setDance, soloDances} = this.props;
    const danceControls = [];
    for (let property in student) {
      if (!property.includes('Level')) {
        continue;
      }

      const danceName = labels[property.slice(0, property.lastIndexOf('Level'))];
      if ((danceName === 'Light Jig' || danceName === 'Single Jig') && student[property] === 'Open') {
        continue;
      }

      let additionalLevel;
      switch (student[property]) {
        case 'Beginner': {
          additionalLevel = 'Primary';
          break;
        }
        case 'Primary': {
          additionalLevel = 'Intermediate';
          break;
        }
        case 'Intermediate': {
          if (danceName !== 'Light Jig' && danceName !== 'Single Jig') {
            additionalLevel = 'Open';
          }
          break;
        }
      }

      let mainLevelCheckBoxValue = false;
      if (soloDances) {
        mainLevelCheckBoxValue = soloDances.find(soloDance => {
          return soloDance=== `${danceName} ${student[property]}`;
        }) !== undefined;
      }

      let additionalLevelCheckBoxValue = false;
      if (soloDances) {
        additionalLevelCheckBoxValue = soloDances.find(soloDance => {
          return soloDance=== `${danceName} ${additionalLevel}`;
        }) !== undefined;
      }

      danceControls.push(
        <div className="dance-controls__item" key={`${danceName} ${student[property]}`}>
          <FormControl>
            <Typography>{danceName}</Typography>
            <FormGroup row>
              <FormControlLabel
                control={createCheckbox(danceName, student[property])}
                label={student[property]}/>
              {additionalLevel
                ? <FormControlLabel
                  control={createCheckbox(danceName, additionalLevel)}
                  label={additionalLevel}/>
                : null}
            </FormGroup>
          </FormControl>
        </div>
      );
    }

    if (danceControls.length >= 2) {
      danceControls.push(createCupDanceItem(student))
    }

    return (
      <div>{danceControls}</div>
    );
  }
}

DanceControls.propTypes = {
  student: PropTypes.shape({
    id: PropTypes.string.isRequired,
    reelLevel: PropTypes.string.isRequired,
    lightLevel: PropTypes.string.isRequired,
    slipLevel: PropTypes.string.isRequired,
    singleLevel: PropTypes.string.isRequired,
    trebleLevel: PropTypes.string.isRequired,
    hornpipeLevel: PropTypes.string.isRequired,
    tradSetLevel: PropTypes.string.isRequired,
    wonPrelimsCount: PropTypes.number.isRequired,
  }).isRequired,
  setDance: PropTypes.func.isRequired,
  soloDances: PropTypes.arrayOf(PropTypes.string.isRequired),
};

export default DanceControls;
