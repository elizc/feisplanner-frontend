import React from "react";
import PropTypes from 'prop-types';
import {CircularProgress} from "@material-ui/core";
import theme from "../../utils/theme";
import ThemeProvider from "@material-ui/styles/ThemeProvider";
import './Spinner.css';

const Spinner = ({size}) => {
  const spinner = <div className="spinner">
    <CircularProgress size={size} color="primary"/>
  </div>;
  return <ThemeProvider theme={theme}>{spinner}</ThemeProvider>
}

Spinner.propTypes = {
  size: PropTypes.number.isRequired
}

export default Spinner;
