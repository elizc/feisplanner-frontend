import React from 'react';
import PropTypes from 'prop-types';
import clsx from "clsx";
import './Flag.css';

const Flag = ({langCode, onClick}) => {
  const classList = clsx('flag', `flag_country_${langCode}`);

  return(
    <div className={classList} onClick={onClick}/>
  )
};

Flag.propTypes = {
  langCode: PropTypes.oneOf(['ru', 'en', 'de']),
  onClick: PropTypes.func.isRequired,
};

export default Flag;
