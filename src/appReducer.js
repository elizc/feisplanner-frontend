import {combineReducers} from "redux";
import {i18nReducer} from "react-redux-i18n";
import registerReducer from './organisms/register-form/reducer';
import loginReducer from './organisms/login-form/reducer';
import schoolsReducer from './organisms/schools-table/reducer';
import studentsReducer from './organisms/students-table/reducer';
import feisesReducer from './organisms/admin-feises-table/reducer';
import teacherFeisesReducer from './organisms/teacher-feises-table/reducer';
import teacherEntriesReducer from './organisms/teacher-entries-table/reducer';
import teacherMenuReducer from './atoms/teacher-menu/reducer';
import orgEntriesReducer from './organisms/org-entries-table/reducer';
import timetableReducer from './organisms/timetable-form/reducer';
import docReducer from './pages/docs-page/reducer';
import ageGroupsReducer from './organisms/age-groups-list/reducer';

const appReducer = combineReducers({
  i18n: i18nReducer,
  registerReducer,
  loginReducer,
  schoolsReducer,
  studentsReducer,
  feisesReducer,
  teacherFeisesReducer,
  teacherEntriesReducer,
  teacherMenuReducer,
  orgEntriesReducer,
  timetableReducer,
  docReducer,
  ageGroupsReducer
});

export default (state = {}, action) => {
  return appReducer(state, action);
};
