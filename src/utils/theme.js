import createMuiTheme from "@material-ui/core/styles/createMuiTheme";

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#008800'
    },
    secondary: {
      main: '#fbc02d'
    }
  },
  zIndex: {
    drawer: 1000
  }
});

export default theme;
