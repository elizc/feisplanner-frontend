export const constants = {
  JWT_KEY: 'elizc@feisplanner.ru',
  FIRST_NAME_KEY: 'fname',
  SURNAME_KEY: 'sname',
  USER_ROLE_KEY: 'roles',
  USER_ID_KEY: 'uid'
};

export const roles = {
  ROLE_ADMIN: 'ROLE_ADMIN',
  ROLE_USER: 'ROLE_USER'
};
