import {I18n} from 'react-redux-i18n';

export function exists(value) {
  return value === '' || value === undefined || value === null ?
    I18n.t('errorText.fillThisField') :
    '';
}

export function containsOnlyLatinLetters(value) {
  const regExp = /[a-z]/;

  return !hasLetters(value.toLowerCase(), regExp) ||
    hasOtherLetters(value.toLowerCase(), regExp) ?
    I18n.t('errorText.onlyLatinLetters') :
    '';
}

export function isPassword(value) {
  const MIN_PASSWORD_LENGTH = 6;
  if (value.length < MIN_PASSWORD_LENGTH) {
    return I18n.t('errorText.passwordLength');
  }

  const necessaryCharacters = [/[a-z]/, /[A-Z]/, /\d/];
  for (let i = 0; i < necessaryCharacters.length; i++) {
    let regExp = necessaryCharacters[i];
    if (!hasLetters(value, regExp)) {
      return I18n.t('errorText.passwordChars');
    }
  }

  if (hasCharsOfNationalAlphabet(value)) {
    return I18n.t('errorText.incorrectChars');
  }

  return '';
}

export function isEmail(value) {
  if (hasCharsOfNationalAlphabet(value)) {
    return I18n.t('errorText.incorrectChars');
  }

  if (!value.includes('@')) {
    return I18n.t('errorText.emailChars');
  }

  const atIndex = value.indexOf('@');
  if (!value.substr(atIndex).includes('.')) {
    return I18n.t('errorText.emailChars');
  }

  return '';
}

export function containsLatinLetters(value) {
  const regExp = /[a-z]/;
  return !hasLetters(value.toLowerCase(), regExp) ?
    I18n.t('errorText.latinLetters') :
    '';
}

export function notContainsNationalChars(value) {
  return hasCharsOfNationalAlphabet(value) ?
    I18n.t('errorText.incorrectChars') :
    '';
}

export function notContainsNumbers(value) {
  const regExp = /\d/;
  return hasLetters(value, regExp) ?
    I18n.t('errorText.noDigits') :
    '';
}

export function notEmpty(value) {
  return value.length > 0 ?
    '' :
    I18n.t('errorText.selectedValues');
}

function hasLetters(value, regExp) {
  return value.match(regExp) !== null;
}

function hasOtherLetters(value, regExp) {
  const otherLetters = value.toLowerCase().split(regExp);
  for (let string of otherLetters) {
    if (string !== "") return true;
  }
  return false;
}

function hasCharsOfNationalAlphabet(value) {
  for (let i = 0; i < value.length; i++) {
    if (value.codePointAt(i) > 127) {
      return true;
    }
  }

  return false;
}
