export const hasAtLeast = (array, value, count) => {
  return array.filter(item => item === value).length >= count
};

export const average = array => {
  return array.reduce((sum, item) => sum + item) / array.length;
};
