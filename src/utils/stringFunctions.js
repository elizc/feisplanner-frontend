import React from "react";
import {I18n} from "react-redux-i18n";

export function makeFirstLettersBig(value) {
  const words = value.split(' ');
  for (let i = 0; i < words.length; i++) {
    const word = words[i];
    const firstLetter = word.substr(0, 1).toUpperCase();
    const rest = word.substr(1, value.length - 1).toLowerCase();
    words[i] = `${firstLetter}${rest}`;
  }
  return words.join(' ');
}

export function getLang() {
  let lang = window.navigator ? (window.navigator.language ||
    window.navigator.systemLanguage ||
    window.navigator.userLanguage) : 'en';

  lang = lang.substr(0, 2).toLowerCase();
  if (lang !== 'en' && lang !== 'de' && lang !== 'ru') {
    lang = 'en';
  }

  return lang;
}

export function getPaginationText({from, to, count}) {
  return `${from}-${to === -1 ? count : to} ${I18n.t('of')} ${count}`;
}

export function getFeisIdFromOrgUrl(url) {
  const lastSlashIndex = url.lastIndexOf('/');
  return url.substr(lastSlashIndex + 1);
}
