export default class Language {
  static ENGLISH = {
    code: 'en',
    nativeName: 'English'
  };
  static GERMAN = {
    code: 'de',
    nativeName: 'Deutsch'
  };
  static RUSSIAN = {
    code: 'ru',
    nativeName: 'Русский'
  };
}
