export const getAge = date => {
  const birthYear = +date.slice(6);
  const currentYear = new Date().getFullYear();
  return currentYear - birthYear - 1;
};

export const registrationIsOpen = feis => {
  const startDate = Date.parse(feis.startDate);
  const now = new Date();
  const daysDiff = (startDate - now) / (1000 * 60* 60 * 24);
  return daysDiff > 14;
};

export const getRegistrationClosingDate = feis => {
  const startDate = new Date(Date.parse(feis.startDate));
  return startDate.setDate(startDate.getDate() - 14);
};
